# OpenAV -- _an_ _experiment_ _in_ _control_

## Intent
To make a new type of control system that leverages open source software products, allowing the system to be vendor and technology agnostic.

### Why
1. Will run anywhere -- The main control system is a Django web application written in Python and can be deployed to any server that can run Python
2. The main control system presents a REST api via websockets -- This allows the programmer to choose the language they prefer. Any program that can connect via websockets could be use to work with the system.
3. The control system is completely web based, driver software can run on the same server or any computer that can reach the main system. This allows for local systems as well as cloud based control.


### Concepts

There are some arbitrary names for concepts used in the system. To assist with explanation, we will be using an example system that includes _driver software_ controlling a networked controlled video projector. The projector has Power, Mute, and Volume controls. 



* Devices
    * Description
        * A database representation of a controllable device.
        * Allows _drivers_ to be able to mark individual _devices_ off or on line
    * Example
        * The _device_ is the projector which is marked on or off line by the _driver_.

* Talents
    * Description
        * A talent is a database representation of an action that a _device_ supports.  
    * Example
        * The Mute control of the projector would be created as a _talent_
        * The Power control of the projector would be created as a _talent_
        * The Volume control of the projector would be created as a _talent_ with _is\_range_ set as True.
        
* Driver 
    * Description
        * A driver is a database representation of the _driver software_
    * Example
        * The software controlling the video projector will register as a _driver_
    
* Panelstyle
    * Description
        * A panelstyle is a database representation of a webcontrol panel
        * The _panelstyle_ links _talents_ in a specific order to a web page

* Driver Software    
    * Description
        * An independent program that connects to the main control system.
    * Actions
        * Receive and maintain a universally unique identifier (UUID) from the main control system.
        * Create any _devices_ in the main control system that it is controlling.
        * Create any _talents_ in the main control system that its _devices_ have.
        * Subscribe to any _talents_ that it is controlling
    * Responsibilities
        * Keep its watchdog updated, or the main control system will mark it off line
        * Update any of it's devices as online or offline as required
        * Respond to it's _talents_ changes to _set\_on_ / _set\_off_ / _range_requested_value_
        * Update the _state_ / _range_value_ of any of it's _talents_ 
    * Example
        * Software that connects to the main control system and also connects to the video projector.

* Main Control System
    * Description
        * A [Django](https://www.djangoproject.com/) web application.
        * Leverages the following Django projects
            * [Django Channels](https://channels.readthedocs.io/)
            * [Django REST framework](https://www.django-rest-framework.org/)
            * [Django Channels REST Framework](https://github.com/hishnash/djangochannelsrestframework)
            * [Django Simple History](https://django-simple-history.readthedocs.io/en/latest/)
    * Actions
        * Listen for connections via http(s) / websockets
        * Regularly check drivers watchdog to maintain online status
    * Responsibilities
        * Security
        * Audit logging
        * Displaying historical records
        * Allowing configuration of _panelstyles_


### Example series of events for _driver software_ adding the projectors driver, device, and talents

* Adding a new Driver
    1. The OpenAV administrator logs into the main control system to generate a new driver user which creates an access token
    2. _Driver software_ connects via websockets and sets header with token
    3. _Driver software_ creates a _driver_ database object with the following attributes
        * owner -- automatically assigned by token
        * name -- Unique name of the _driver software_ (Example Projector Driver)
    4. _Django server_ saves _driver_ which automatically creates the drivers UUID
    5. _Django server_ sends _create_ message which includes the _driver\_id_ and _driver_uuid_
    6. _Driver software_ _retrieves_ the _driver\_id_ and waits until _authorized_ is _True_
    7. The administrator logs into the main control system and authorizes the _driver_
    8. _Driver software_ sees that it is _authorized_
    9. _Driver software_ creates a _device_ database object with the following attributes
        * unique_id -- Unique ID for the _driver software_ to reference the _device_
        * name -- User changeable name of the _device_ (Example Projector)
        * description -- Optional description (Projector mounted on the north side of the room)
        * is_online -- Current state of _device_
    10. _Django server_ sends _create_ action which includes the _device_ _id_
    11. _Driver software_ creates a _talent_ database object with the following attributes
        * name -- User changeable name of the _talent_ this name is displayed on the web control panel
        * description -- Optional description of this _talent_
        * in_transition -- Optional attribute to indicate that the _talent_ is moving from one state to another
        * disable_if_in_transition -- Optional attribute to indicate if the _talent_ can accept commands while _in\_transition_
        * driver -- The UUID of the _driver software_
    12. _Django server_ sends _create_ action which includes the _talent_ _id_
    13. _Driver software_ sends a _subscribe_instance_ to monitor the _talent_ for changes
         
        
### Example series of events for powering on the projector from the web control panel

* Powering on the projector
    1. A user clicks the Power button on the web control panel
    2. Web control panel _patches_ the Power _talent_ _set\_on_ to _True_
    3. Driver software is notified via subscription to the _talent_ of the change to _set\_on_
    4. Driver software _patches_ the Power _talent_ _set\_on_ to _False_
    5. Driver software sends the appropriate commands to the video projector
    6. Driver software receives conformation that the projector has powered on.
    7. Driver software _patches_ the Power _talent_ _state_ to _True_
    8. Web control panel changes the Power button on the web page to show the power is on.


### Advantages

* Main control system Django
    * Open source software
    * Runs on most operating systems
    * Secure
    * Scalable

* Driver software
    * Written by others
    * Security enforced by main control system
    * Responsibilities enforced by main control system
    * Can be written in any language
    * Limited access

* Web control pages
    * Default control pages provided
    * Custom control pages can be written
    * Make a change on a panelstyle and all panels are immediately updated.
