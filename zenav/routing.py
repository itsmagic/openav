from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import plankowner.routing
from .token_auth import TokenAuthMiddleware

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        TokenAuthMiddleware(
            URLRouter(
                plankowner.routing.websocket_urlpatterns
            )
        ),
    ),
})
