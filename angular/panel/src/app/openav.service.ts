import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Observable, Observer, timer, Subject, EMPTY, of } from 'rxjs';
import { retryWhen, tap, delayWhen, switchAll, catchError } from 'rxjs/operators';
import { Device, Driver, PanelStyle, Talent } from './model';
import { Router } from '@angular/router';
import { TalentsComponent } from './talents/talents.component';

@Injectable({
  providedIn: 'root'
})

export class OpenavService {

  // private socket: WebSocketSubject<any> =  webSocket('ws://10.0.0.22:8000/ws/multi/');
  private socket: WebSocketSubject<any>; 
  private messagesSubject = new Subject();
  public messages = this.messagesSubject;
  public panelStyles: PanelStyle[] = [];
  public talents: Talent[] = [];
  public devices: Device[] = [];
  public drivers: Driver[] = [];
  public paneltalents: Talent[] = [];
  public panel: PanelStyle = {"name": "", "id": 0, "talent_order": "", "talents": []};
  // public buttons: Talent[] = [];
  // public ranges: Talent[] = [];

  
  // ws$ = webSocket('ws://10.0.0.22:8000/ws/multi/');
  // mypanelstyle: PanelStyle = {};

  constructor(private router: Router) {
    var ws_protocol = "ws:"
    if (window.location.protocol === "https:") {
            ws_protocol = "wss:"
    }
    this.socket = webSocket(ws_protocol  + '//' + window.location.host + '/ws/multi/')
   }

  ngOnInit(){
    
  }

  connect() {
    this.socket
      .subscribe(
        msg => this.process_message(msg)
      )
      // const messages = this.socket.pipe(
      //   tap({
      //     error: error => console.log(error),
      //   }), catchError(_ => EMPTY));
      // this.process_message(messages);
    // this.get_panelstyle(1)

    
    
  }

  process_message(msg: any) {
    switch (msg.stream){
      case 'panelstylestream': {
        this.panelsytlestream(msg)
        break
        }
      case 'talentstream': {
        this.talentstream(msg)
        break
      }
      case 'driverstream': {
        this.driverstream(msg)
        break
      }
      case 'devicestream': {
        this.devicestream(msg)
        break
      }
      default: {
        console.log('not panel', msg)
      }
    }
  }

  panelsytlestream(msg: any){
    switch (msg.payload.action) {
      case 'retrieve':{
        console.log(msg)
        this.panel = msg.payload.data
        if (msg.payload.request_id == 1){
          this.send({"stream": "talentstream", "payload": {"action": "list", "request_id": 43}})
        } else {
          msg.payload.data.talents.forEach((item: number) => {
            this.send({"stream": "talentstream", "payload": {"action": "retrieve", "pk": item, "request_id": 43}})
          }); 
        }
        break
      }
      case 'list': {
        msg.payload.data.forEach((item: PanelStyle) => {
          if (!this.updateOrfalse(item, this.panelStyles)){
            // subscribe
            this.send({"stream": "panelstylestream", "payload": {"action": "subscribe_instance", "pk": item.id, "request_id": 43}})
            // add to approiate list
            this.panelStyles.push(item)
          }
        })
        break
      }
      default: {
      }
    }
  }

  talentstream(msg: any){
    switch (msg.payload.action) {
      case 'retrieve':
      case 'update': {
        if (!this.updateOrfalse(msg.payload.data, this.talents)){
          // subscribe
          this.send({"stream": "talentstream", "payload": {"action": "subscribe_instance", "pk": msg.payload.data.id, "request_id": 43}})
          // add to approiate list
          this.talents.push(msg.payload.data)
          // if (!msg.payload.data.is_range) {
          //   this.buttons.push(msg.payload.data)
          // } else {
          //   this.ranges.push(msg.payload.data)
          // }
          // get device
          this.send({"stream": "devicestream", "payload": {"action": "retrieve", "pk": msg.payload.data.device.id, "request_id": 43}})
        }
        var talent_order = this.panel.talent_order.replace(/, +/g, ",").split(",").map(Number);
        if (this.talents.length == talent_order.length){
          console.log("Organizing")
          var temp: Talent[] = []
              talent_order.forEach((idx: number) => {
                temp.push(this.talents[idx])
              })
              // console.log("talent_order", talent_order)
              // console.log("temp", temp)
              
              // console.log("this.paneltalents", this.paneltalents)
              this.talents.length = 0
              temp.forEach(thing => {
                this.talents.push(thing)
              })
        }
        break
      }
      case 'list': {
        msg.payload.data.forEach((item: Talent) => {
          // console.log("testing panelids", item.id, this.paneltalentids, (this.paneltalentids.includes(item.id)))
          if (this.panel.talents.includes(item.id)) {
            if (!this.updateOrfalse(item, this.paneltalents)) {
              this.paneltalents.push(item)
              this.send({"stream": "talentstream", "payload": {"action": "subscribe_instance", "pk": item.id, "request_id": 43}})
            }
            var talent_order = this.panel.talent_order.replace(/, +/g, ",").split(",").map(Number);
            if (this.paneltalents.length == talent_order.length) {
              // console.log("We have the same number organizeing")
              // 6,2,3,7,5,4,8,0,9,1,10
              var temp: Talent[] = []
              talent_order.forEach((idx: number) => {
                temp.push(this.paneltalents[idx])
              })
              // console.log("talent_order", talent_order)
              // console.log("temp", temp)
              
              // console.log("this.paneltalents", this.paneltalents)
              this.paneltalents.length = 0
              temp.forEach(thing => {
                this.paneltalents.push(thing)
              })
              // this.paneltalents.forEach((talent: Talent) => {
              //   console.log("this.panelids", talent.id)
              // })
              // this.paneltalents = temp
              // console.log("after")
              // console.log("temp", temp)
              // console.log("this.paneltalents", this.paneltalents)
            }
            // console.log(this.paneltalents)
          } else {
            if (!this.updateOrfalse(item, this.talents)) {
              this.talents.push(item)
              this.send({"stream": "talentstream", "payload": {"action": "subscribe_instance", "pk": item.id, "request_id": 43}})
            }
          }
        })
        break
      }
    }
  }

  devicestream(msg: any){
    switch (msg.payload.action) {
      case 'retrieve': {
        // subscribe
        this.send({"stream": "devicestream", "payload": {"action": "subscribe_instance", "pk": msg.payload.data.id, "request_id": 43}})
        // add to list
        this.devices.push(msg.payload.data)
        this.drivers.push(msg.payload.data.driver)
        // console.log(this.drivers)
        // get device
        //this.send({"stream": "devicestream", "payload": {"action": "retrieve", "pk": msg.payload.data.driver.id, "request_id": 43}})
        break
      }
      case 'update': {
        // all we are looking for is on / offline
        // Check if any of our talents are owned by this device
        this.talents.forEach((talent: Talent) => {
          if (talent.device.id == msg.payload.data.id) {
            this.send({"stream": "talentstream", "payload": {"action": "retrieve", "pk": talent.id, "request_id": 43}})
          }
        });
      }
    }
  }

  driverstream(msg: any){

  }

  send(msg: any) {
    this.socket.next(msg);
  }

  // get_panel(id: number) {
  //   this.get_panelstyle(id)
  // }

  get_panelstyle(id: number, component: number) {
    this.socket.next({"stream": "panelstylestream", "payload": {"action": "retrieve", "pk": id, "request_id": component}})
  }

  get_allpanelstyles() {
    this.socket.next({"stream": "panelstylestream", "payload": {"action": "list", "request_id": 43}})
  }

  get_alltalents(){
    this.socket.next({"stream": "talentstream", "payload": {"action": "list", "request_id": 42 }})
  }

  patch_panelstyle(pk: Number, talents: Number[], talent_order: String){
    this.socket.next({"stream": "panelstylestream", "payload": {"action": "patch", "data": {"talents": talents, "talent_order": talent_order}, "pk": pk, "request_id": 43}})
  }

  // getTalents(): Observable<Talent[]> {
  //   return of(this.talents)
  // }

  getTalents(): Observable<Talent[]> {
    return of(this.talents)
  }

  getPanelStyles(): Observable<PanelStyle[]> {
    return of(this.panelStyles)
  }

  getPanelTalents(): Observable<Talent[]> {
    return of(this.paneltalents)
  }

  getPanel(): Observable<PanelStyle> {
    return of(this.panel)
  }

  // getRanges(): Observable<Talent[]> {
  //   return of(this.ranges)
  // }

  getPanelStyleById(id: Number) {
    var panel: PanelStyle = {"id": 1, "name": "", "talents": [], "talent_order": ""};
    this.panelStyles.forEach(item => {
      if (item.id == id) {
        panel = item;
      };
    });
    return panel
  }

  updateOrfalse(obj: any, list: any) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].id === obj.id) {
            list.splice(i, 1, obj)
            // console.log('updated: ')
            // console.log(obj)
            return true;
        }
    }

    return false;
  }
}
