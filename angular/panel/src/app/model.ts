export interface PanelStyle {
    id: number;
    name: string;
    talent_order: string;
    talents: number[]; 
}

export interface Driver {
    description: string;
    devices: number[];
    driver_uuid: string;
    id: number;
    is_online: boolean;
    name: string;
}

export interface Talent {
    name: string;
    id: number;
    state: boolean;
    in_transition: boolean;
    is_range: boolean;
    range_value: number;
    range_min: number;
    range_max: number;
    requested_range_value: number;
    device: Device;
}

export interface Device {
    id: number;
    name: string;
    is_online: boolean;
    driver: Driver;

}


