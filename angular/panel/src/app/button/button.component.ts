import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Talent } from '../model';
import { OpenavService } from '../openav.service';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  talents: Talent[] = [];
  ranges: Talent[] = [];
  message: string = "";
  messageShown: Boolean = false;
  selectedButton?: Talent;

  constructor(
    private openav: OpenavService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.openav.connect();
    this.getPanel();
  }

  getPanel(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.openav.get_panelstyle(id, 2)
    // this.getTalents()
    // this.getRanges()
    this.monitorTalents()

  }

  onSelectButton(talent: Talent): void {
    // console.log(talent.state)
    if (!talent.device.is_online){
      this.messageShown = true
      this.message = "This device is not on line"
    }
    if (talent.state) {
      this.openav.send({"stream": "talentstream", "payload": {"action": "patch", "data": {"set_off": true}, "pk": talent.id, "request_id": 43}})
    } else {
      this.openav.send({"stream": "talentstream", "payload": {"action": "patch", "data": {"set_on": true}, "pk": talent.id, "request_id": 43}})
    }
  }

  onSelectRange(talent: Talent, value: string): void {
    // console.log(talent)
    // console.log(value)
    this.openav.send({"stream": "talentstream", "payload": {"action": "patch", "data": {"requested_range_value": value}, "pk": talent.id, "request_id": 43}})
  }

  monitorTalents(): void {
    this.openav.getTalents()
      .subscribe(talents => this.talents = talents)
  }

  // getRanges(): void {
  //   this.openav.getRanges()
  //     .subscribe(ranges => this.ranges = ranges)
  // }

}
