import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ButtonComponent } from './button/button.component';
import { TalentsComponent } from './talents/talents.component';

const routes: Routes = [
  { path: 'config/:id', component: TalentsComponent },
  { path: 'panel/:id', component: ButtonComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }