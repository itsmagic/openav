import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Talent, PanelStyle } from '../model';
import { OpenavService } from '../openav.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-talents',
  templateUrl: './talents.component.html',
  styleUrls: ['./talents.component.css']
})
export class TalentsComponent implements OnInit {

  offPanel: Talent[] = [];

  onPanel: Talent[] = [];
  panelStyles: PanelStyle[] = [];
  panel!: PanelStyle;



  drop(event: CdkDragDrop<Talent[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    // Update web
    const id = Number(this.route.snapshot.paramMap.get('id'));
    var talents: number[] = []
    var talent_order: Number[] = []
    // console.log("onPanel", this.onPanel)
    this.onPanel.forEach(item => {
      // We need to update the web talents order and items
      talents.push(item.id)

    })
    var sortedArray = [...talents]
    sortedArray.sort((n1,n2) => n1 - n2);
    talents.forEach(item => {
      talent_order.push(sortedArray.indexOf(item))
    })

    // console.log("talents", talents)
    // console.log("sortedArray", sortedArray)
    // console.log("talent_order", talent_order)
    this.openav.patch_panelstyle(id, talents, talent_order.join(","))
  }


  constructor(
    private openav: OpenavService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.openav.connect();
    // this.getAllPanelStyles();
    this.monitorTalents();
    this.monitorPanelTalents();
    this.monitorPanel();
    this.getPanelTalents();
    // this.getAllTalents();
    

  }

  

  // getAllTalents(): void {
  //   // const id = Number(this.route.snapshot.paramMap.get('id'));
  //   // this.openav.get_alltalents()
  //   this.monitorTalents()
  //   // this.getRanges()

  // }

  // getAllPanelStyles(): void {
  //   // const id = Number(this.route.snapshot.paramMap.get('id'));
  //   this.openav.get_allpanelstyles()
  //   this.monitorPanelStyles()
  // }

  getPanelTalents(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.openav.get_panelstyle(id, 1)

  }

  monitorPanelTalents(): void {
    this.openav.getPanelTalents()
      .subscribe(talents => {
        this.onPanel = talents
    })
  }

  monitorTalents(): void {
    this.openav.getTalents()
      .subscribe(talents => {
        this.offPanel = talents})
  }

  monitorPanel(): void {
    this.openav.getPanel()
      .subscribe(panel => {
        this.panel = panel
      })
  }

  // monitorPanelStyles(): void {
  //   this.openav.getPanelStyles()
  //     .subscribe(panelStyles => {
  //       this.panelStyles = panelStyles
  //     })
  // }

}
