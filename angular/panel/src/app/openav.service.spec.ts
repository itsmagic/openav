import { TestBed } from '@angular/core/testing';

import { OpenavService } from './openav.service';

describe('OpenavService', () => {
  let service: OpenavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenavService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
