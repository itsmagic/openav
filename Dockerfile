FROM python:3

ENV DJANGO_DB_NAME=default
ENV DJANGO_SU_NAME=admin
ENV DJANGO_SU_EMAIL=admin@openav.example.com
ENV DJANGO_SU_PASSWORD=letmein

WORKDIR /code/source
RUN mkdir /code/source/static
RUN mkdir /code/source/media
RUN mkdir /code/database

ENV PYTHONUNBUFFER=1


COPY requirements.txt /code/source/
# COPY demo_db.sqlite3 /code/database/db.sqlite3

RUN pip install --upgrade -r requirements.txt

COPY . /code/source/

# RUN python -c "import os; os.environ['DJANGO_SETTINGS_MODULE'] = 'zenav.settings';\
#    import django; django.setup(); \
#    from django.contrib.auth.management.commands.createsuperuser import get_user_model; \
#    get_user_model()._default_manager.db_manager('$DJANGO_DB_NAME').create_superuser( \
#    username='$DJANGO_SU_NAME', \
#    email='$DJANGO_SU_EMAIL', \
#    password='$DJANGO_SU_PASSWORD')"
