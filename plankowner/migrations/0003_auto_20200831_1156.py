# Generated by Django 3.1 on 2020-08-31 01:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plankowner', '0002_videoroute_talent'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='videoroute',
            name='videooutput',
        ),
        migrations.AddField(
            model_name='videoroute',
            name='videooutput',
            field=models.ManyToManyField(to='plankowner.VideoOutput'),
        ),
    ]
