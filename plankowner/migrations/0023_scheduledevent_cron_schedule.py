# Generated by Django 3.1 on 2021-07-13 01:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plankowner', '0022_scheduledevent'),
    ]

    operations = [
        migrations.AddField(
            model_name='scheduledevent',
            name='cron_schedule',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
