# Generated by Django 3.1 on 2020-11-24 02:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plankowner', '0016_historicaldriver_watchdog'),
    ]

    operations = [
        migrations.RenameField(
            model_name='historicaltalent',
            old_name='disable_if_in_transistion',
            new_name='disable_if_in_transition',
        ),
        migrations.RenameField(
            model_name='historicaltalent',
            old_name='in_transistion',
            new_name='in_transition',
        ),
        migrations.RenameField(
            model_name='talent',
            old_name='disable_if_in_transistion',
            new_name='disable_if_in_transition',
        ),
        migrations.RenameField(
            model_name='talent',
            old_name='in_transistion',
            new_name='in_transition',
        ),
    ]
