
from django.urls import path, re_path
from rest_framework.urlpatterns import format_suffix_patterns
from plankowner import views

urlpatterns = [
    path('devices/', views.DeviceList.as_view()),
    path('devices/<int:pk>/', views.DeviceDetail.as_view()),
    path('drivers/', views.DriverList.as_view()),
    path('drivers/<int:pk>/', views.DriverDetail.as_view()),
    path('driverconfigs/', views.DriverConfigList.as_view()),
    path('driverconfigs/<int:pk>/', views.DriverConfigDetail.as_view()),
    path('talents/', views.TalentList.as_view()),
    path('talents/<int:pk>/', views.TalentDetail.as_view()),
    path('paneltalents/', views.PanelTalentList.as_view()),
    path('paneltalents/<int:pk>/', views.PanelTalentDetail.as_view()),
    path('videoinputs/', views.VideoInputList.as_view()),
    path('videoinputs/<int:pk>/', views.VideoInputDetail.as_view()),
    path('videooutputs/', views.VideoOutputList.as_view()),
    path('videooutputs/<int:pk>/', views.VideoOutputDetail.as_view()),
    path('videoroutes/', views.VideoRouteList.as_view()),
    path('videoroutes/<int:pk>/', views.VideoRouteDetail.as_view()),
    path('macroactions/', views.MacroActionList.as_view()),
    path('macroactions/<int:pk>/', views.MacroActionDetail.as_view()),
    path('macros/', views.MacroList.as_view()),
    path('macros/<int:pk>/', views.MacroDetail.as_view()),
    path('panelstyles/', views.PanelStyleList.as_view()),
    path('panelstyles/<int:pk>/', views.PanelStyleDetail.as_view()),
    path('rooms/', views.RoomList.as_view()),
    path('rooms/<int:pk>/', views.RoomDetail.as_view()),
    path('watchdog/<int:pk>/', views.watchdog),
    path('panel/<int:pk>', views.view_panel, name='panel_detail'),
    path('panel/<int:pk>/<str:output>', views.view_panel),
    path('multipanel/', views.view_multipanel),
    # path('unassigned', views.admin_view.as_view()),
    path('', views.demo_panels, name='home'),
    path('app/', views.app_authorization),
    # path('ang/<int:pk>', views.angular_panel),
    path('plot', views.view_plot),
    re_path(r'^(?P<path>.*)/$', views.angular_panel),
]

urlpatterns = format_suffix_patterns(urlpatterns)
