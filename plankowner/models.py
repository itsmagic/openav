from django.db import models
from django.utils import timezone
# from django.core.exceptions import ValidationError
import datetime
import uuid

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from simple_history.models import HistoricalRecords
import logging

logger = logging.getLogger(__name__)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Driver(models.Model):
    driver_uuid = models.UUIDField(default=uuid.uuid4)
    created = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=True, default='')
    owner = models.ForeignKey('auth.User', related_name='drivers', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    authorized = models.BooleanField(default=False)
    is_online = models.BooleanField(default=False)
    watchdog = models.DateTimeField(default=timezone.now)
    unresponsive = models.BooleanField(default=False)
    response_timeout = models.IntegerField(default=30)
    history = HistoricalRecords()

    def update_watchdog(self):
        self.watchdog = timezone.now()
        self.save_without_historical_record()

    def check_watchdog(self, history_user=None):

        if is_unresponsive_driver(self, self.response_timeout):
            # We need to mark it unresponsive
            if not self.unresponsive:
                self.unresponsive = True
                self.is_online = False
                self.save()
            # Since we are unresponsive we do not need to check watchdog time
            return
        else:
            if self.unresponsive:
                logger.critical(f"Clearing unresponsive for {self}")
                self.unresponsive = False
                self.save()

        if timezone.now() > self.watchdog + datetime.timedelta(minutes=1):
            if self.is_online:
                self.is_online = False
                self._history_user = history_user
                self.save()
        else:
            if not self.is_online:
                self.is_online = True
                self._history_user = history_user
                self.save()

    def save_without_historical_record(self, *args, **kwargs):
        """Use this when marking self online"""
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def save(self, *args, **kwargs):
        # self.check_watchdog()
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'


class DriverConfig(models.Model):
    driver = models.ForeignKey(Driver, related_name='configs', on_delete=models.CASCADE)
    key = models.CharField(max_length=100, blank=True, default='')
    config_value = models.TextField(blank=True, default='')

    def __str__(self):
        return f'{self.driver.name}: {self.key}'


class Device(models.Model):
    driver = models.ForeignKey(Driver, related_name='devices', on_delete=models.CASCADE)
    unique_id = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, default='')
    name = models.CharField(max_length=100)
    is_online = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.driver}: {self.name}'


class Talent(models.Model):
    device = models.ForeignKey(Device, related_name='talents', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    # owner = models.ForeignKey('auth.User', related_name='talents', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, default='')
    state = models.BooleanField(default=False)
    in_transition = models.BooleanField(default=False)
    disable_if_in_transition = models.BooleanField(default=False)
    is_range = models.BooleanField(default=False)
    range_value = models.IntegerField(default=0)
    range_min = models.IntegerField(default=0)
    range_max = models.IntegerField(default=100)
    requested_range_value = models.IntegerField(default=0)

    set_on = models.BooleanField(default=False)
    set_off = models.BooleanField(default=False)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.name}'


class Building(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, default='')

    def __str__(self):
        return f'{self.name}'


class Room(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, default='')
    building = models.ForeignKey(Building, related_name='rooms', null=True, blank=True, on_delete=models.SET_NULL)
    talents = models.ManyToManyField(Talent, blank=True)

    def __str__(self):
        return f'{self.name}'


class VideoInput(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    device = models.ForeignKey(Device, related_name='videoinputs', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    description = models.TextField(blank=True, default='')

    def __str__(self):
        return f'{self.name} -- {self.device}'


class VideoOutput(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    device = models.ForeignKey(Device, related_name='videooutputs', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    description = models.TextField(blank=True, default='')

    def __str__(self):
        return f'{self.name} -- {self.device}'


class VideoRoute(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    videoinput = models.ForeignKey(VideoInput, related_name='videoroutes', null=True, blank=True, on_delete=models.SET_NULL)
    videooutputs = models.ManyToManyField(VideoOutput, related_name='videoroutes', blank=True)
    talent = models.ForeignKey(Talent, related_name='route_talent', null=True, blank=True, on_delete=models.SET_NULL)
    description = models.TextField(blank=True, default='')

    def __str__(self):
        # video_outputs = [output.name for output in self.videooutputs.all()]
        # if self.videoinput is None:
        #     vi = None
        # else:
        #     vi = self.videoinput.name
        return f'{self.name}'  # ': {vi} --> {video_outputs}'


class MacroAction(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    talent = models.ForeignKey(Talent, related_name='macroaction_talent', blank=True, on_delete=models.CASCADE)

    requested_range_value = models.IntegerField(default=0)
    set_on = models.BooleanField(default=False)
    set_off = models.BooleanField(default=False)

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'


class Macro(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, default='')
    remove_delays_on_cancel = models.BooleanField(default=False)
    macro_order = models.CharField(max_length=100)
    macro_actions = models.ManyToManyField(MacroAction, related_name='macros', blank=True)
    talent = models.ForeignKey(Talent, related_name='macros', null=True, blank=True, on_delete=models.SET_NULL)

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'


class ScheduledEvent(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, default='')
    cron_schedule = models.CharField(blank=True, max_length=100)
    enabled = models.BooleanField(default=False)
    one_shot = models.BooleanField(default=False)
    talent = models.ForeignKey(Talent, related_name='scheduledevents', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.name}'


class PanelStyle(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, default='')
    talents = models.ManyToManyField(Talent, blank=True)
    talent_order = models.CharField(max_length=100, blank='', default='')
    enabled = models.BooleanField(default=True)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.name}'


def is_unresponsive_driver(driver, timeout):
    unresponsive_talents = Talent.objects.filter(device__driver__id=driver.id, set_on=True) | Talent.objects.filter(device__driver__id=driver.id, set_off=True)
    # logger.critical(f"Found these unresponsive talents: {unresponsive_talents}")
    for talent in unresponsive_talents:
        # logger.critical(f"Checking {talent} set_on={talent.set_on} set_off={talent.set_off}")
        # logger.critical(f"Cal waiting time {timezone.now()} - {talent.history.first().history_date} = {timezone.now() - talent.history.first().history_date}")
        request_waiting_time = timezone.now() - talent.history.first().history_date
        # logger.critical(f"Checking if request_waiting_time is greater than {timeout} seconds {request_waiting_time} > {datetime.timedelta(seconds=timeout)}")
        if request_waiting_time > datetime.timedelta(seconds=timeout):
            logger.critical(f"Found unresponsive driver: {driver} is not responding to {talent}")
            return True
    return False
