from djangochannelsrestframework import permissions


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to view it.
    """

    def has_object_permission(self, scope, view, list, obj):
        # print(scope)
        # Read permissions are allowed to any request,
        # # so we'll always allow GET, HEAD or OPTIONS requests.
        if scope.method in permissions.SAFE_METHODS:
            # Check permissions for read-only request
            return obj.driver__owner == scope.user
        else:
            return obj.driver__owner == scope.user
            # Check permissions for write request
        # if request.method in permissions.SAFE_METHODS:
        #     return obj.owner == scope.user

        # Write permissions are only allowed to the owner of the snippet.
        # return obj.driver__owner != scope.user
        # return obj.pk == 7

