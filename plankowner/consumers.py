from .models import *

from djangochannelsrestframework import permissions
from djangochannelsrestframework.generics import GenericAsyncAPIConsumer
from djangochannelsrestframework.mixins import CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, DeleteModelMixin
from djangochannelsrestframework.observer.generics import ObserverModelInstanceMixin
from djangochannelsrestframework.observer import model_observer
from rest_framework import status

from djangochannelsrestframework.decorators import action
from channels.db import database_sync_to_async
from channels.auth import login, get_user
from channelsmultiplexer import AsyncJsonWebsocketDemultiplexer

from channels.consumer import SyncConsumer

from . import serializers
import logging

logger = logging.getLogger(__name__)


class WatchDogConsumer(GenericAsyncAPIConsumer):
    queryset = Driver.objects.all()
    serializer_class = serializers.DriverSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @action()
    async def update_watchdog(self, driver, **kwargs):
        driver = await database_sync_to_async(self.get_object)(pk=driver)
        # driver.update_watchdog()
        await database_sync_to_async(driver.update_watchdog)()
        # remember to wrap all db actions in `database_sync_to_async`
        return None, status.HTTP_200_OK  # return the content and the response code.

    @action()
    async def check_watchdog(self, **kwargs):
        await login(self.scope, self.scope['user'])
        my_user = await get_user(self.scope)
        # logger.critical(f"Check watchdog called by {my_user}")
        all_drivers = await self.get_all_drivers()
        await self.check_watchdog_on_drivers(all_drivers, my_user)
        return None, status.HTTP_200_OK

    @database_sync_to_async
    def get_all_drivers(self):
        return Driver.objects.all()

    @database_sync_to_async
    def check_watchdog_on_drivers(self, drivers, user):
        for driver in drivers:
            driver.check_watchdog(history_user=user)
            # driver.save() This is done in check watchdog...


class DriverConsumer(ObserverModelInstanceMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = Driver.objects.all()
    serializer_class = serializers.DriverSerializer
    permission_classes = (permissions.IsAuthenticated,)

    # def get_queryset(self, **kwargs):
    #     return Driver.objects.filter(owner=self.scope['user'])

    def perform_create(self, serializer, **kwargs):
        serializer.save(owner=self.scope['user'])

    def perform_patch(self, serializer, **kwargs):
        if serializer.instance.owner != self.scope['user']:
            raise PermissionError
        serializer.save()


class DriverConfigConsumer(ObserverModelInstanceMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = DriverConfig.objects.all()
    serializer_class = serializers.DriverConfigSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self, **kwargs):
        return DriverConfig.objects.filter(driver__owner=self.scope['user'])

    def perform_create(self, serializer, driver, **kwargs):
        # print(driver)
        my_driver = Driver.objects.get(driver_uuid=driver)
        if my_driver.owner != self.scope['user']:
            raise PermissionError
        serializer.save(driver=my_driver)


class DeviceConsumer(ObserverModelInstanceMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, DeleteModelMixin, GenericAsyncAPIConsumer):
    queryset = Device.objects.all()
    serializer_class = serializers.DeviceSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def perform_patch(self, serializer, **kwargs):
        serializer.instance._history_user = self.scope['user']
        # logger.critical(f"user is : {my_user} and type is {type(my_user)}")
        serializer.save()

    def perform_create(self, serializer, driver, **kwargs):
        # print(driver)
        my_driver = Driver.objects.get(driver_uuid=driver)
        if my_driver.owner != self.scope['user']:
            logger.critical(f"Incorrect permissions in perform_create:  {driver.owner} != {self.scope['user']}")
            raise PermissionError
        serializer.save(driver=my_driver)

    def perform_delete(self, instance, **kwargs):
        if instance.driver.owner != self.scope['user']:
            logger.critical(f"Incorrect permissions in perform_delete:  {instance.driver.owner} != {self.scope['user']}")
            raise PermissionError
        instance.delete()

class TalentConsumer(ObserverModelInstanceMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, DeleteModelMixin, GenericAsyncAPIConsumer):
    queryset = Talent.objects.all()
    serializer_class = serializers.TalentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_patch(self, serializer, **kwargs):
        serializer.instance._history_user = self.scope['user']
        # logger.critical(f"user is : {serializer.instance._history_user} and type is {type(serializer.instance._history_user)}")
        serializer.save()

    def perform_create(self, serializer, device, **kwargs):
        # print(device)
        my_device = Device.objects.get(pk=device)
        if my_device.driver.owner != self.scope['user']:
            raise PermissionError
        serializer.save(device=my_device)


class PanelTalentConsumer(ObserverModelInstanceMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = Talent.objects.all()
    serializer_class = serializers.TalentSerializer
    permission_classes = (permissions.IsAuthenticated,)


class PanelStyleConsumer(ObserverModelInstanceMixin, ListModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = PanelStyle.objects.all()
    serializer_class = serializers.PanelStyleSerializer
    permission_classes = (permissions.IsAuthenticated,)


class VideoInputConsumer(ObserverModelInstanceMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = VideoInput.objects.all()
    serializer_class = serializers.VideoInputSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer, device, **kwargs):
        # print(device)
        my_device = Device.objects.get(pk=device)
        if my_device.driver.owner != self.scope['user']:
            raise PermissionError
        serializer.save(device=my_device)


class VideoOutputConsumer(ObserverModelInstanceMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = VideoOutput.objects.all()
    serializer_class = serializers.VideoOutputSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer, device, **kwargs):
        # print(device)
        my_device = Device.objects.get(pk=device)
        if my_device.driver.owner != self.scope['user']:
            raise PermissionError
        serializer.save(device=my_device)


class VideoRouteConsumer(ObserverModelInstanceMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = VideoRoute.objects.all()
    serializer_class = serializers.VideoRouteSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_patch(self, serializer, talent, **kwargs):
        my_talent = Talent.objects.get(pk=talent)
        if my_talent.device.driver.owner != self.scope['user']:
            raise PermissionError
        serializer.save(talent=my_talent)
    
    @model_observer(VideoRoute)
    async def model_activity(self, message, action=None, **kwargs):
        payload = {'data': message,
                   'action': action}
        await self.send_json(payload)

    @action()
    async def monitor(self, **kwargs):
        await self.model_activity.subscribe()
        return None, status.HTTP_201_CREATED

    @model_activity.serializer
    def model_serialize(self, instance, action, **kwargs):
        return serializers.VideoRouteSerializer(instance).data


class MacroActionConsumer(ObserverModelInstanceMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = MacroAction.objects.all()
    serializer_class = serializers.MacroActionSerializer
    permission_classes = (permissions.IsAuthenticated,)


class MacroConsumer(ObserverModelInstanceMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = Macro.objects.all()
    serializer_class = serializers.MacroSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_patch(self, serializer, talent, **kwargs):
        my_talent = Talent.objects.get(pk=talent)
        if my_talent.device.driver.owner != self.scope['user']:
            raise PermissionError
        serializer.save(talent=my_talent)

    @model_observer(Macro)
    async def model_activity(self, message, action=None, **kwargs):
        payload = {'data': message,
                   'action': action}
        await self.send_json(payload)

    @action()
    async def monitor(self, **kwargs):
        await self.model_activity.subscribe()
        return None, status.HTTP_201_CREATED

    @model_activity.serializer
    def model_serialize(self, instance, action, **kwargs):
        return serializers.MacroSerializer(instance).data


class ScheduledEventConsumer(ObserverModelInstanceMixin, ListModelMixin, UpdateModelMixin, PatchModelMixin, GenericAsyncAPIConsumer):
    queryset = ScheduledEvent.objects.all()
    serializer_class = serializers.ScheduledEventSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @model_observer(ScheduledEvent)
    async def model_activity(self, message, action=None, **kwargs):
        payload = {'data': message,
                   'action': action}
        await self.send_json(payload)

    @action()
    async def monitor(self, **kwargs):
        await self.model_activity.subscribe()
        return None, status.HTTP_201_CREATED
        

    @model_activity.serializer
    def model_serialize(self, instance, action, **kwargs):
        return serializers.ScheduledEventSerializer(instance).data


class RoomConsumer(ObserverModelInstanceMixin, ListModelMixin, GenericAsyncAPIConsumer):
    queryset = Room.objects.all()
    serializer_class = serializers.RoomSerializer
    permission_classes = (permissions.IsAuthenticated,)


class TestConsumer(SyncConsumer):

    def websocket_connect(self, event):
        self.send({
            "type": "websocket.accept",
        })

    def websocket_receive(self, event):
        print(f"event: {event}")
        self.send({
            "type": "websocket.send",
            "bytes": event["bytes"],
        })


class PanelDemultiplexerAsyncJson(AsyncJsonWebsocketDemultiplexer):
    applications = {
        "talentstream": TalentConsumer.as_asgi(),
        "devicestream": DeviceConsumer.as_asgi(),
        "driverstream": DriverConsumer.as_asgi(),
        "driverconfigstream": DriverConfigConsumer.as_asgi(),
        "videoinputstream": VideoInputConsumer.as_asgi(),
        "videooutputstream": VideoOutputConsumer.as_asgi(),
        "videoroutestream": VideoRouteConsumer.as_asgi(),
        "macrostream": MacroConsumer.as_asgi(),
        "scheduledeventstream": ScheduledEventConsumer.as_asgi(),
        "panelstylestream": PanelStyleConsumer.as_asgi(),
        "roomstream": RoomConsumer.as_asgi(),
        "watchdogstream": WatchDogConsumer.as_asgi()
        # "teststream": TestConsumer
    }

