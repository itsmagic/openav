from django.contrib import admin

from simple_history.admin import SimpleHistoryAdmin
from .models import *


class DriverHistoryAdmin(SimpleHistoryAdmin):
    list_display = ["name", "owner", "authorized", "is_online", "unresponsive", "watchdog"]
    history_list_display = ["is_online"]
    search_fields = ['name', 'is_online']


class DeviceHistoryAdmin(SimpleHistoryAdmin):
    list_display = ["name", "is_online", "driver"]
    history_list_display = ["is_online"]
    search_fields = ['name', 'is_online']


class TalentHistoryAdmin(SimpleHistoryAdmin):
    list_display = ["name", "device", "state", "set_on", "set_off", "in_transition", "is_range", "range_value"]
    history_list_display = ["state", "in_transition"]
    search_fields = ['name', 'device__name', 'device__driver__name']


class ScheduledEventAdmin(admin.ModelAdmin):
    list_display = ["name", "cron_schedule", "enabled", "one_shot", "talent"]


class MacroAdmin(admin.ModelAdmin):
    list_display = ["name", "remove_delays_on_cancel", "macro_order", "actions_count", "talent"]

    def actions_count(self, obj):
        count = obj.macro_actions.count()
        return f"{count}"


class MacroActionAdmin(admin.ModelAdmin):
    list_display = ["name", "requested_range_value", "set_on", "set_off", "linked_to", "talent"]

    def linked_to(self, obj):
        count = obj.macros.count()
        return f"{count}"


class PanelStyleAdmin(admin.ModelAdmin):
    list_display = ["name", "talents_count", "talent_order", "enabled"]

    def talents_count(self, obj):
        count = obj.talents.count()
        return f"{count}"


class VideoRouteAdmin(admin.ModelAdmin):
    list_display = ["name", "videoinput", "talent"]


admin.site.register(Driver, DriverHistoryAdmin)
admin.site.register(Device, DeviceHistoryAdmin)
admin.site.register(Talent, TalentHistoryAdmin)
admin.site.register(VideoRoute, VideoRouteAdmin)

admin.site.register(ScheduledEvent, ScheduledEventAdmin)
admin.site.register(Macro, MacroAdmin)
admin.site.register(MacroAction, MacroActionAdmin)

admin.site.register(Building)
admin.site.register(PanelStyle, PanelStyleAdmin)
admin.site.register(Room)
admin.site.register(VideoInput)
admin.site.register(VideoOutput)
admin.site.register(DriverConfig)

# # Register your models here.
# # all other models
# models = apps.get_models()

# for model in models:
#     try:
#         admin.site.register(model)
#     except admin.sites.AlreadyRegistered:
#         pass
