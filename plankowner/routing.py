from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    # re_path(r'ws/driver/', consumers.DriverConsumer),
    # re_path(r'ws/driverconfig/', consumers.DriverConfigConsumer),
    # re_path(r'ws/device/', consumers.DeviceConsumer),
    # re_path(r'ws/talent/', consumers.TalentConsumer),
    # re_path(r'ws/panelstyle/', consumers.PanelStyleConsumer),
    re_path(r'ws/test/', consumers.TestConsumer.as_asgi()),
    re_path(r'ws/multi/', consumers.PanelDemultiplexerAsyncJson.as_asgi())
]
