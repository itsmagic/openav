from rest_framework import serializers
# from rest_framework_recursive.fields import RecursiveField
# from rest_framework.fields import UUIDField
from plankowner.models import *


class DriverSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    driver_uuid = serializers.UUIDField(read_only=True)
    authorized = serializers.ReadOnlyField()

    class Meta:
        model = Driver
        fields = ['name', 'owner', 'description', 'is_online', 'authorized', 'id', 'driver_uuid', 'devices']


class DriverConfigSerializer(serializers.ModelSerializer):

    class Meta:
        model = DriverConfig
        fields = ['key', 'config_value', 'id', 'driver']


class DeviceSerializer(serializers.ModelSerializer):
    driver = DriverSerializer(read_only=True)

    class Meta:
        model = Device
        fields = ['name', 'unique_id', 'description', 'is_online', 'id', 'driver']


class TalentSerializer(serializers.ModelSerializer):
    # owner = serializers.ReadOnlyField(source='device.driver.owner.username')
    device = DeviceSerializer(read_only=True)

    class Meta:
        model = Talent
        fields = ['name', 'description', 'created', 'set_on', 'set_off', 'is_range', 'range_value',
                  'requested_range_value', 'range_min', 'range_max', 'state',
                  'in_transition', 'disable_if_in_transition', 'id', 'device']


class VideoInputSerializer(serializers.ModelSerializer):

    device = DeviceSerializer(read_only=True)

    class Meta:
        model = VideoInput
        fields = ['name', 'number', 'description', 'id', 'device', 'videoroutes']


class VideoOutputSerializer(serializers.ModelSerializer):

    device = DeviceSerializer(read_only=True)

    class Meta:
        model = VideoOutput
        fields = ['name', 'number', 'description', 'id', 'device', 'videoroutes']

class VideoRouteSerializer(serializers.ModelSerializer):

    videoinput = VideoInputSerializer(read_only=True)
    videooutputs = VideoOutputSerializer(many=True, read_only=True)
    talent = TalentSerializer(read_only=True)

    class Meta:
        model = VideoRoute
        fields = ['name', 'videoinput', 'videooutputs', 'talent', 'id']

class MacroActionSerializer(serializers.ModelSerializer):

    talent = TalentSerializer(read_only=True)

    class Meta:
        model = MacroAction
        fields = ['name', 'requested_range_value', 'set_on', 'set_off', 'talent', 'id']

class MacroSerializer(serializers.ModelSerializer):

    macro_actions = MacroActionSerializer(many=True, read_only=True)
    talent = TalentSerializer(read_only=True)

    class Meta:
        model = Macro
        fields = ['name', 'description', 'remove_delays_on_cancel', 'macro_order', 'macro_actions', 'talent', 'id']

class ScheduledEventSerializer(serializers.ModelSerializer):

    talent = TalentSerializer(read_only=True)

    class Meta:
        model = ScheduledEvent
        fields = ['name', 'description', 'cron_schedule', 'enabled', 'one_shot', 'talent', 'id']

class PanelStyleSerializer(serializers.ModelSerializer):

    class Meta:
        model = PanelStyle
        fields = ['name', 'id', 'talents', 'talent_order', 'enabled']

class BuildingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Building
        fields = ['name', 'description']


class RoomSerializer(serializers.ModelSerializer):
    # subareas = serializers.StringRelatedField(many=True)
    # talents = serializers.StringRelatedField(many=True)
    building = BuildingSerializer(read_only=True)

    class Meta:
        model = Room
        fields = ['name', 'description', 'talents', 'building']
