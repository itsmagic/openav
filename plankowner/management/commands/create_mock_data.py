from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from plankowner.models import *
import random
import uuid

class Command(BaseCommand):
    help = 'Creates mock data'

    def handle(self, *args, **options):
        # Create users
        users = ['Liyana', 'Amelia', 'Skylar', 'Jared', 'Alesha', 'driver']
        for user in users:
            if not User.objects.filter(username=user).exists():
                User.objects.create_user(username=user,
                                         email=f'{user}@ornear.com',
                                         password='1' + user[:3] + 'open')
        talents_names = ['HDMI', 'DVI', 'Power', 'Analog', 'Volume', 'Brightness']
        range_names = ['Volume', 'Brightness']
        driver_pk = User.objects.get(username='driver')
        # Create Drivers, devices, and talents
        driver_devices = {'Cat': {'Leopard': random.choices(talents_names, k=random.randrange(5)),
                                  'Jaguar': random.choices(talents_names, k=random.randrange(5)),
                                  'Cougar': random.choices(talents_names, k=random.randrange(5)),
                                  'Cheetah': random.choices(talents_names, k=random.randrange(5))},
                          'Dog': {'Shepherd': random.choices(talents_names, k=random.randrange(5)),
                                  'Retriever': random.choices(talents_names, k=random.randrange(5)),
                                  'Beagle': random.choices(talents_names, k=random.randrange(5))},
                          'Bird': {'Fairywren': random.choices(talents_names, k=random.randrange(5)),
                                   'Magpie': random.choices(talents_names, k=random.randrange(5)),
                                   'Lorikeet': random.choices(talents_names, k=random.randrange(5))},
                          'Turtle': {},
                          'Fish': {'Tuna': random.choices(talents_names, k=random.randrange(5))}}
        for driver in driver_devices.keys():
            # Create driver
            my_driver, created = Driver.objects.get_or_create(name=driver, owner=driver_pk)
            for device in driver_devices[driver]:
                my_device, created = Device.objects.get_or_create(name=device, driver=my_driver, unique_id=str(uuid.uuid4()))
                for talent in driver_devices[driver][device]:
                    my_talent, created = Talent.objects.get_or_create(name=talent, device=my_device)
                    if created and my_talent.name in range_names:
                        my_talent.is_range = True
                        my_talent.range_min = 0
                        my_talent.range_max = random.randrange(100)
                        my_talent.save()
        area_names = {'Queensland': ['Gold Coast', 'Brisbane', 'Townsville'],
                      'NSW': ['Sydney', 'Byron Bay', 'Coffs Harbor'],
                      'Victoria': ['Melbourne']
                      }
        for building in area_names:
            my_building, created = Building.objects.get_or_create(name=building)
            for room in area_names[building]:
                my_room, created = Room.objects.get_or_create(name=room, building=my_building)
