from channels.testing import WebsocketCommunicator
from plankowner.consumers import WatchDogConsumer, DriverConsumer
from django.contrib.auth.models import User
from plankowner.models import Driver
import pytest
from django.conf.urls import url
from channels.routing import URLRouter
from channels.db import database_sync_to_async
import json


from channels.generic.websocket import (
    AsyncJsonWebsocketConsumer,
    AsyncWebsocketConsumer,
    JsonWebsocketConsumer,
    WebsocketConsumer,
)


application = URLRouter([
    url(r"^ws/test/", DriverConsumer),
])


@pytest.mark.skip
@pytest.mark.django_db
@pytest.mark.asyncio
async def test_websocket_consumer():
    """
    Tests that WebsocketConsumer is implemented correctly.
    """
    user, driver = await create_driver()
    # print(driver)
    # Test a normal connection
    communicator = WebsocketCommunicator(application, f"/ws/test/")
    connected, _ = await communicator.connect()
    assert connected
    # Test sending text
    await communicator.send_json_to({'action': 'list', 'data': {}, 'request_id': 42})
    response = await communicator.receive_json_from()
    # (response)
    assert response['data'][0]['name'] == 'test_driver'
    # Close out
    await communicator.disconnect()


@pytest.mark.django_db
@database_sync_to_async
def create_driver():
    user = User.objects.create_user(username='testuser', password='12345')
    # scoreboard = ScoreBoard.objects.create(name='test')
    # team = Team.objects.create(name='test team', scoreboard=scoreboard)
    # score = Score.objects.create(team=team)
    # return scoreboard, team, score
    driver = Driver.objects.create(name='test_driver', owner=user)
    return user, driver


# @pytest.mark.django_db
# @database_sync_to_async
# def update_score(score):
#     score = Score.objects.get(id=score.id)
#     score.round_1 = 5
#     score.save()
