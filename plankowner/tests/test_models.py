from plankowner.models import Driver, Device, Talent
from django.test import TestCase
from django.contrib.auth.models import User
from django.utils import timezone
import datetime
import time
import logging

logger = logging.getLogger(__name__)


class WatchDogCase(TestCase):
    def setUp(self):
        my_user = User.objects.create_user(username="mqtt user", password='12345')
        my_driver = Driver.objects.create(name="mqtt", owner=my_user)
        my_device = Device.objects.create(name="arlec twin", driver=my_driver)
        Talent.objects.create(name="lamp", device=my_device)

    def test_unresponsive_driver_marked_offline_if_set_on_left_on(self):
        """Tests that a drive is marked as unresponsive when set_on is left on"""
        driver = Driver.objects.get(name='mqtt')
        driver.response_timeout = 1
        driver.save()
        # device = Device.objects.get(name='arlec twin')
        talent = Talent.objects.get(name='lamp')
        talent.set_on = True
        talent.save()
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertTrue(driver.is_online)
        self.assertFalse(driver.unresponsive)
        time.sleep(2)
        driver.check_watchdog()
        self.assertFalse(driver.is_online)
        self.assertTrue(driver.unresponsive)
        talent.set_on = False
        talent.save()
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertTrue(driver.is_online)
        self.assertFalse(driver.unresponsive)

    def test_unresponsive_driver_marked_offline_if_set_off_left_on(self):
        """Tests that a drive is marked as unresponsive when set_on is left on"""
        driver = Driver.objects.get(name='mqtt')
        driver.response_timeout = 1
        driver.save()
        # device = Device.objects.get(name='arlec twin')
        talent = Talent.objects.get(name='lamp')
        talent.set_off = True
        talent.save()
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertTrue(driver.is_online)
        self.assertFalse(driver.unresponsive)
        time.sleep(2)
        driver.check_watchdog()
        self.assertFalse(driver.is_online)
        self.assertTrue(driver.unresponsive)
        talent.set_off = False
        talent.save()
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertTrue(driver.is_online)
        self.assertFalse(driver.unresponsive)

    def test_unresponsive_driver_marked_offline_if_set_off_and_set_on_left_on(self):
        """Tests that a drive is marked as unresponsive when set_on is left on"""
        driver = Driver.objects.get(name='mqtt')
        driver.response_timeout = 1
        driver.save()
        # device = Device.objects.get(name='arlec twin')
        talent = Talent.objects.get(name='lamp')
        talent.set_off = True
        talent.set_on = True
        talent.save()
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertTrue(driver.is_online)
        self.assertFalse(driver.unresponsive)
        time.sleep(2)
        driver.check_watchdog()
        self.assertFalse(driver.is_online)
        self.assertTrue(driver.unresponsive)
        talent.set_off = False
        talent.save()
        time.sleep(2)
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertFalse(driver.is_online)
        self.assertTrue(driver.unresponsive)
        talent.set_on = False
        talent.save()
        time.sleep(2)
        driver.update_watchdog()
        driver.check_watchdog()
        self.assertTrue(driver.is_online)
        self.assertFalse(driver.unresponsive)
    
