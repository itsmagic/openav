from plankowner.models import *
from rest_framework.authtoken.models import Token
# Driver, Device, Talent, DriverConfig, VideoInput
from plankowner.serializers import *
# DriverSerializer, DeviceSerializer, TalentSerializer, PanelStyleSerializer, DriverConfigSerializer, VideoInputSerializer
from rest_framework import generics, permissions
from plankowner.permissions import IsOwnerOrReadOnly  #, IsOwner

from rest_framework.decorators import api_view
from rest_framework.response import Response

# from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from .models import PanelStyle
from django.http import JsonResponse
from django.views.generic import ListView
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import login
import random
import string
import logging
import datetime
import pytz
import json

# import matplotlib.pyplot as plt
# import matplotlib.dates
# import numpy as np
# import pandas as pd
# import mpld3
# from mpld3 import plugins

from django.utils import timezone

from django.conf import settings

logger = logging.getLogger(__name__)

# A pytz.timezone object representing the Django project time zone
# Use TZ.localize(mydate) instead of tzinfo=TZ to ensure that DST rules
# are respected
# TZ = timezone(settings.TIME_ZONE)


# class demo_view(ListView):
#     model = PanelStyle
#     context_object_name = "panels"

def angular_panel(request, path, format=None):
    return render(request, 'plankowner/angular_index.html')


def app_authorization(request):
    if not request.user.is_authenticated or request.user.username == "anonymous_panel":
        return redirect("home")
    token = Token.objects.filter(user__username="anonymous_panel").first()
    print(f"checking {request.GET.get('name', '')} got {token}")
    if token:
        my_ws_url = request.build_absolute_uri().replace(request.get_full_path(), "").replace("https", "wss").replace("http", "ws") + "/ws/multi/"
        qr_text = f"{token.key}\n{my_ws_url}"
        context = {"token": token, "qr_text": qr_text, "ws_text": my_ws_url}
    else:
        context = {}
    return render(request, 'plankowner/app_authorization.html', context)

def demo_panels(request):
    panels = PanelStyle.objects.all()
    panel_uris = []
    for panel in panels:
        panel_uris.append((panel, request.build_absolute_uri(f"/panel/{panel.id}")))
    context = {"panel_uris": panel_uris}
    if hasattr(settings, "WIFI_ACCESS"):
        context['wifi'] = settings.WIFI_ACCESS
    # , "wifi": settings.WIFI_ACCESS}
    return render(request, 'plankowner/panelstyle_list.html', context)

class admin_view(ListView):
    model = Room
    context_object_name = "rooms"



def view_plot(request):
    talents = Talent.objects.filter(is_range=False, name="String Lights")
    # events = np.array([])
    # dates = np.array([])
    # result = [{'name': 'On', 'data': []}, {'name': 'Off', 'data': []}]
    # for talent in talents:
    #     events = []
    #     dates = []
    #     for item in reversed(talent.history.filter(history_date__range=[datetime.date(2021, 6, 9), datetime.date(2021, 6, 11)])):
    #         # events = np.append(events, item.state)
    #         # dates = np.append(dates, item.history_date)
    #         events.append(int(item.state))
    #         dates.append(item.history_date)
    #         # strftime('%Y-%m-%d %H:%M:%S.000Z'))
    #         # 2020-11-12T04:00:01.443980+00:00Z
    #         # 2013-02-08 09:30:26.123+07:00

    #     # print(events)
    #     # print(dates)
        
    #     current_value = None
    #     start_date = None
    #     for i, value in enumerate(events):
    #         if current_value is None:
    #             # Must be the first one...
    #             current_value = value
    #             start_date = dates[i]
    #             continue
    #         if value != current_value:
    #             # We have changed state

    #             result[value]['data'].append({'x': talent.name, 'y': [start_date, dates[i]]})
    #             current_value = value
    #             start_date = dates[i]
    #         # passing no status change
    #     # Add current date as final value
    #     result[current_value]['data'].append({'x': talent.name, 'y': [start_date, timezone.now()]})
    # print(result)
    result = []
    for j, talent in enumerate(talents):
        events = []
        dates = []
        result.append({"data": [], "name": talent.name})
        for item in reversed(talent.history.filter(history_date__range=[datetime.datetime(2021, 2, 16, 0, 0, 0, 0, pytz.UTC), datetime.datetime(2021, 6, 22, 0, 0, 0, 0, pytz.UTC)])):
            events.append(int(item.state))
            dates.append(item.history_date)
        print(events)
        # print([date.isoformat() for date in dates])
        current_value = None
        start_date = None
        for i, value in enumerate(events):
            result[j]['data'].append({"x": (dates[i] + datetime.timedelta(hours=10)).timestamp() * 1000, "y": value})
        #     if current_value is None:
        #         # Must be the first one...
        #         current_value = value
        #         start_date = dates[i]
        #         # result[0]['data'].append({"x": start_date.timestamp() * 1000, "y": current_value})
        #         continue
        #     if value != current_value:
        #         # We have changed state
        #         # Mark the end of old state
        #         # result[0]['data'].append({"x": start_date.timestamp() * 1000, "y": current_value})
        #         # Mark the start of new state
        #         result[0]['data'].append({"x": (start_date + datetime.timedelta(hours=10)).timestamp() * 1000, "y": value})
        #         current_value = value
        #         start_date = dates[i]
        #     # passing no status change
        # # Add current date as final value
        # if current_value is not None:
        #     result[0]['data'].append({"x": (datetime.datetime.now() + datetime.timedelta(hours=10)).timestamp() * 1000, "y": value})
        with open('data.json', 'w') as f:
            json.dump(result, f)
        print(result)

    context = {'title': 'My Chart', 'series': result}
    return render(request, 'plankowner/plot2.html', context)


def view_panel(request, pk, output='html'):
    # my_panel = PanelStyle.objects.get(id=pk)
    if not request.user.is_authenticated:
        if settings.USE_ANONYMOUS_PANEL_ACCESS:
            try:
                default_user = User.objects.get(username=settings.ANONYMOUS_PANEL_USERNAME)
            except User.DoesNotExist:
                logger.warning(f"Adding a anonymous panel user")
                default_user = User.objects.create_user(settings.ANONYMOUS_PANEL_USERNAME,
                                                        'anonymous@example.com',
                                                        ''.join(random.choice(string.ascii_letters + string.digits) for i in range(26)))
            login(request, default_user)

    context = {'panelstyle': pk, 'authenticated': (str(request.user.is_authenticated)).lower()}
    if output == 'json':
        # print('sending json')
        return JsonResponse(context)
    else:
        return render(request, 'plankowner/panel.html', context)


def view_multipanel(request):
    # my_panel = PanelStyle.objects.get(id=pk)
    if not request.user.is_authenticated:
        if settings.USE_ANONYMOUS_PANEL_ACCESS:
            try:
                default_user = User.objects.get(username=settings.ANONYMOUS_PANEL_USERNAME)
            except User.DoesNotExist:
                logger.warning(f"Adding a anonymous panel user")
                default_user = User.objects.create_user(settings.ANONYMOUS_PANEL_USERNAME,
                                                        'anonymous@example.com',
                                                        ''.join(random.choice(string.ascii_letters + string.digits) for i in range(26)))
            login(request, default_user)

    panels = []
    for panel in PanelStyle.objects.all():
        panels.append(panel.id)

    context = {'panels': panels, 'authenticated': (str(request.user.is_authenticated)).lower()}

    return render(request, 'plankowner/multipanel.html', context)

# class PanelView(TemplateView):
#     template_name = "plankowner/panel.html"

#     def get_context_data(self, *args, **kwargs):
#         context = super(PanelView, self).get_context_data(*args, **kwargs)
#         context['buttons'] = ['one', 'two', 'three', 'four', 'five', 'six']
#         context['button_names'] = ['Lights']
#         return context


@api_view(['POST'])
def watchdog(request, pk):
    my_driver = Driver.objects.filter(id=pk).first()
    my_driver.update_watchdog()
    my_driver.save()
    # my_resp = DriverSerializer(request, my_driver).is_valid()
    return Response({'update': 'success'})


class DriverList(generics.ListCreateAPIView):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    # def list(self, request):
    #     # Note the use of `get_queryset()` instead of `self.queryset`
    #     if self.request.user.id is None:
    #         queryset = Driver.objects.none()
    #     else:
    #         queryset = Driver.objects.filter(owner=self.request.user)
    #     serializer = DriverSerializer(queryset, many=True)
    #     return Response(serializer.data)


class DriverDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class DriverConfigList(generics.ListCreateAPIView):
    queryset = DriverConfig.objects.all()
    serializer_class = DriverConfigSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class DriverConfigDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DriverConfig.objects.all()
    serializer_class = DriverConfigSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class DeviceList(generics.ListCreateAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    # def list(self, request):
    #     # Note the use of `get_queryset()` instead of `self.queryset`
    #     if self.request.user.id is None:
    #         queryset = Device.objects.none()
    #     else:
    #         queryset = Device.objects.filter(driver__owner=self.request.user)
    #     serializer = DeviceSerializer(queryset, many=True)
    #     return Response(serializer.data)


class DeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class TalentList(generics.ListCreateAPIView):
    queryset = Talent.objects.all()
    serializer_class = TalentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    # def list(self, request):
    #     if self.request.user.id is None:
    #         queryset = Talent.objects.none()
    #     else:
    #         queryset = Talent.objects.filter(device__driver__owner=self.request.user)
    #     serializer = TalentSerializer(queryset, many=True)
    #     return Response(serializer.data)


class TalentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Talent.objects.all()
    serializer_class = TalentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class PanelTalentList(generics.ListCreateAPIView):
    queryset = Talent.objects.all()
    serializer_class = TalentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def list(self, request):
        if not self.request.user.groups.filter(name='av_panels').exists:
            queryset = Talent.objects.none()
        else:
            # Filter by Room assignment queryset = Talent.objects.filter(room=self.request.user.)
            queryset = self.get_queryset()
        serializer = TalentSerializer(queryset, many=True)
        return Response(serializer.data)


class PanelTalentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Talent.objects.all()
    serializer_class = TalentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class VideoInputList(generics.ListCreateAPIView):
    queryset = VideoInput.objects.all()
    serializer_class = VideoInputSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class VideoInputDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = VideoInput.objects.all()
    serializer_class = VideoInputSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class VideoOutputList(generics.ListCreateAPIView):
    queryset = VideoOutput.objects.all()
    serializer_class = VideoOutputSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class VideoOutputDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = VideoOutput.objects.all()
    serializer_class = VideoOutputSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class VideoRouteList(generics.ListCreateAPIView):
    queryset = VideoRoute.objects.all()
    serializer_class = VideoRouteSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class VideoRouteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = VideoRoute.objects.all()
    serializer_class = VideoRouteSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class MacroActionList(generics.ListCreateAPIView):
    queryset = MacroAction.objects.all()
    serializer_class = MacroActionSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class MacroActionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MacroAction.objects.all()
    serializer_class = MacroActionSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class MacroList(generics.ListCreateAPIView):
    queryset = Macro.objects.all()
    serializer_class = MacroSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class MacroDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Macro.objects.all()
    serializer_class = MacroSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class PanelStyleList(generics.ListCreateAPIView):
    queryset = PanelStyle.objects.all()
    serializer_class = PanelStyleSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class PanelStyleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PanelStyle.objects.all()
    serializer_class = PanelStyleSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class RoomList(generics.ListCreateAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

