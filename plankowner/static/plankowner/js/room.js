
var off_color = "264653"
var on_color = "2A9D8F"
var error_color = "E76F51"
var start_color = "e5e5e5"
var selected_room_id = null;

var ws_protocol = "ws:"

if (window.location.protocol === "https:") {
        ws_protocol = "wss:"
}

$( document ).ready(function() {

    

    let ws = new ReconnectingWebSocket(ws_protocol + '//' + window.location.host + '/ws/multi/');

    ws.onopen = function() {
        show_success("Connected to Server");
        // Get Talents
        console.log('On open');
        // send_format_data('areastream', "subscribe_instance", [], panelstyle);
        // ws.send(JSON.stringify({'stream': 'roomstream', 'payload': {"action": 'list', "request_id": 42, 'data': []}}));
        ws.send(JSON.stringify({'stream': 'talentstream', 'payload': {"action": 'list', "request_id": 42}}));   
    };

    ws.onclose = function() {
        show_warning("Lost Connection to the server");

    }

    ws.onmessage = function(event) {
        const all_data = JSON.parse(event.data);
        console.log(all_data);
        if (all_data.stream == 'talentstream'){
            if (all_data.payload.action == 'list'){
                //Remove
                $("#talent_container").empty()
                //Add all with description
                all_data.payload.data.forEach(function(talent){
                    $("#talent_container").append('<div class="round_talent" onclick="talent_click()" id="talent_' + talent.id +'"></div>');
                    $("#talent_" + talent.id).append('<p class="item" id="talent_text_' + talent.id + '"></p>');
                    $("#talent_text_" + talent.id).html(talent.device.name + '-->' + talent.name);
                    // $("#talent_container").append('<div class="card text-center" id="card_' + talent.id + '""></div>');
                    // $("#card_" + talent.id).append('<div class="card-header">' + talent.device.driver.name + ' -- ' + talent.device.name + '</div>');
                    // $("#card_" + talent.id).append('<div class="card-body" id="card_body_' + talent.id + '""></div>');
                    // $("#card_body_" + talent.id).append('<h5 class="card-title">' + talent.name + '</h5>');
                    // $("#card_body_" + talent.id).append('<p class="card-text" id="card_text_' + talent.id + '">' + talent.description + '</p>');
                    // // $("#card_body_" + talent.id).append('<a href="#" class="btn btn-primary">Go somewhere</a>');
                    // $("#card_" + talent.id).append('<div class="card-footer text-muted"> Created:  TBD</div>');
                })
                

            }
            
        }
    }

    function send_format_data(stream, action, data, pk){
        // action retrieve subscribe
        ws.send(JSON.stringify({'stream': stream, 'payload': {"action": action, "request_id": 42, 'data': data, "pk": pk}}));
    };
        
    // $(".room").click(function() {
    //     $(".room").removeClass('room_selected');
    //     $(this).addClass('room_selected');
    //     var room_id = event.target.id.replace('room_', '').replace('text_', '');
    //     selected_room_id = room_id;
    //     // console.log("event: " + event.target)
    //     // console.log("Room id: " + room_id)
    //     // console.log("Selected: " + selected_room_id)
        
    // });
    // $(".room_container").click(function(){
    //     console.log("event: " + (event.target));
    // })
    // $(".talent_container").click(function() {
    //     console.log("event: " + (event.target));
    //     var talent_id = event.target.id.replace('text_', '').replace('text_device_', '');
    //     console.log(talent_id)
    //     if (selected_room_id != null) {
    //         $("#talent_" + talent_id).appendTo("#room_" + selected_room_id);
    //     }
    // })
    
});

function talent_click(){
    console.log('Got a talent click');
    console.log(event.target.id);
    console.log("Selected: " + selected_room_id)
    var talent_id = event.target.id.replace('text_', '');
    console.log(talent_id)
    if (selected_room_id != null) {
            $("#" + talent_id).appendTo("#" + selected_room_id);
            // Add talent to room
            send_format_data('roomstream', 'patch', {'talent'})

        }
}
function room_click(){
    console.log('Got a room click');
    console.log(event.target.id);
    if (event.target.id.includes('talent')){
        var talent_id = event.target.id.replace('text_', '');
        console.log(talent_id)
        $("#" + talent_id).appendTo("#talent_container");
        // remove talent from room
        return;
    }
    var room_id = event.target.id.replace('text_', '');
    $(".room").removeClass('room_selected');
    $("#" + room_id).addClass('room_selected');
    selected_room_id = room_id;
    
}


function show_success(message){
    $("div.success").html(message);
    $("div.success").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
};

function show_warning(message){
    $("div.warning").html(message);
    $("div.warning").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
};





