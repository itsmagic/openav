
// const range = new RangeTouch('input[type="range"]', {});

var off_color = "264653"
var on_color = "2A9D8F"
var error_color = "E76F51"
var start_color = "e5e5e5"
var request_id = 1

var subscriptions = {'talents': [], 'drivers': [], 'devices': []}

// //offline
// let offline_devices = new Set();
// let offline_driver_devices = new Set();
var talents_list = [];
var talent_order = [];
// var talent_devices_id_list = [];

var devices_list = [];
// var device_drivers_id_list = [];

var drivers_list = [];
let offline_devices = new Set();
let offline_drivers = new Set();

var talent_local_states = [];

var ws_protocol = "ws:"

if (window.location.protocol === "https:") {
        ws_protocol = "wss:"
}

$( document ).ready(function() {
    if (!is_auth){
        $("#button_container").append('<h1>You will need to login first!</h1>');
        return;
    };

    let ws = new ReconnectingWebSocket(ws_protocol + '//' + window.location.host + '/ws/multi/');

    ws.onopen = function() {
        show_success("Connected to Server");
        // Get Talents
        console.log('Websocket open');
        send_format_data('panelstylestream', "subscribe_instance", [], panelstyle);
        send_format_data('panelstylestream', "retrieve", [], panelstyle);
    };

    ws.onclose = function() {
        show_warning("Lost Connection to the server");
        // Add all drivers to offline
        drivers_list.forEach(function (driver){
            offline_drivers.add(driver.id)
        });
        console.log(offline_drivers);
        console.log('update online off line');
        update_online_offline();

    }

    ws.onmessage = function(event) {
        // console.log(`[message] Data received from server: ${event.data}`);
        const all_data = JSON.parse(event.data);
        if (all_data.payload.errors.length){
            show_warning("Websocket error: " + all_data.payload.errors)
        }

        ws_stream = all_data.stream
        ws_payload = all_data.payload
        ws_action = ws_payload['action']

        switch(ws_stream){

            case 'panelstylestream':
                // console.log('in panelstylestream')
                var panelstyle = ws_payload.data
                
                switch(ws_action){

                    case 'list':
                    case 'create':
                    case 'delete':
                        break;
                    case 'retrieve':
                    case 'update':
                        // We have an update/retrieve to panel style ... need to rebuild the page
                        // Clear out buttons and ranges
                        $('#button_container').empty();
                        $('#range_container').empty();
                        
                        talents_list = [];
                        devices_list = [];
                        drivers_list = [];
                        subscriptions = {'talents': [], 'drivers': [], 'devices': []}
                        //Subscribe and get currentstate of talents
                        // console.log('List of talents should be empty: ')
                        // console.log(talents_list)
                        if (panelstyle.talent_order != ''){
                            var ordered_talents = [];

                            try {
                                    // talent_order = panelstyle.talent_order.split(',');
                                    var talent_order = $.map(panelstyle.talent_order.split(','), function(value){
                                                            return parseInt(value, 10);
                                                        });
                                    if (talent_order.length != panelstyle.talents.length){
                                        ordered_talents = panelstyle.talents;
                                        throw {'message':"length mismatch"};
                                    }

                                    for (let i = 0; i < talent_order.length; i++){
                                        if (isNaN(talent_order[i])){
                                            throw {'message':"not a number"};
                                        }
                                        if (parseInt(talent_order[i]) <= panelstyle.talents.length){
                                            ordered_talents.push(panelstyle.talents[talent_order[i]])
                                        } else {
                                            throw {'message': "index out of range"}
                                        }

                                        
                                    }
                                }
                            catch(err) {
                                  // console.log('Unable to order talents due to error ' + err.message);
                                  show_warning("Unable to order talents, " + err.message)
                                  ordered_talents = panelstyle.talents;
                                }
                        } else {
                            console.log("Unable to order talents because no talent order was provided.")
                            ordered_talents = panelstyle.talents;
                        }

                        ordered_talents.forEach(function (talent) {  //only subscribe if we haven't already
                            // We need to get all talents to build the page every time the panelstyle is updated
                            send_format_data('talentstream', "retrieve", [], talent);
                        });
                        break;
                    default:
                        return
                };
                return

            case 'talentstream':
                // console.log('in talentstylestream');
                var talent = all_data.payload.data;

                switch(ws_action){

                    case 'list':
                    case 'create':
                    case 'delete':
                        break;
                    case 'retrieve':
                    case 'update':
                        // {"stream": "talentstream", "payload": {"errors": [], "data": {"name": "Power", "set_on": false, "set_off": false, "is_range": false, "range_value": 0, "requested_range_value": 0, "range_min": 0, "range_max": 100, "state": false, "in_transition": false, "id": 1, "device": 2}, "action": "retrieve", "response_status": 200, "request_id": 42}}
                        if (!obj_in_list(talent, subscriptions.talents)){
                                console.log('subing to talent ' + talent.id);
                                // console.log(talent);
                                send_format_data('talentstream', "subscribe_instance", [], talent.id);
                                subscriptions.talents.push(talent);
                                send_format_data('devicestream', "retrieve", [], talent.device.id);

                        };
                        if (!updateOrfalse(talent, talents_list)){
                            talents_list.push(talent);
                            talent_local_states.push('False');
                        };
                        //Check if the talents are on the page and create them if necessary
                        if (!talent.is_range){  // its a button
                            
                            // Check if we need to create buttons
                            if (!$("#button_" + talent.id).length) {   //will return false if selector doesn't exist
                                //we need to add
                                $("#button_container").append('<div class="round_button" id=button_' + talent.id +'></div>');
                                $("#button_" + talent.id).append('<div class="button_text" id="button_text_' + talent.id + '"></div>');
                                // $("#button_text_" + talent.id).text(talent.name);
                            };

                            // Update states
                            if (talent.state){
                                talent_local_states[talents_list.indexOf(talent)] = talent.state;
                                $("#button_" + talent.id).addClass("button_on");
                                $("#button_" + talent.id).removeClass("button_off");
                            } else {
                                $("#button_" + talent.id).addClass("button_off");
                                $("#button_" + talent.id).removeClass("button_on");
                            };

                            // Update Name
                            $("#button_text_" + talent.id).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw transistion_hidden"></i><br>' + talent.name + '<br><i class="fa fa-spinner fa-pulse fa-1x fa-fw transistion_hidden"></i>');

                            // Mark transistions
                            if (talent.in_transition){
                                // console.log('in transistions ')
                                if (talent.disable_if_in_transition){
                                    $("#button_" + talent.id).addClass("button_disabled");
                                } else {
                                    // $("#button_text_" + talent.id).addClass("fa-spin");
                                    $("#button_text_" + talent.id).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw transistion_hidden"></i><br>' + talent.name + '<br><i class="fa fa-spinner fa-pulse fa-1x fa-fw transistion_shown"></i>')
                                };
                            } else {
                                // console.log('not transistions')
                                if (talent.disable_if_in_transition){
                                    $("#button_" + talent.id).removeClass("button_disabled");
                                } else {
                                    // $("#button_text_" + talent.id).removeClass("fa-spin");
                                    $("#button_text_" + talent.id).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw transistion_hidden"></i><br>' + talent.name + '<br><i class="fa fa-spinner fa-pulse fa-1x fa-fw transistion_hidden"></i>');
                                };
                            }; 
                            
                            
                        } else {  //its a range
                            
                            // Check if we need to create ranges
                            if (!$("#range_" + talent.id).length) {
                                $("#range_container").append('<div class="form-group" id="form_group_' + talent.id + '"></div>');
                                // $("#form_group_" + talent.id).append('<label for="range_' + talent.id +'" id="range_text_' + talent.id +'"></label>');
                                $("#form_group_" + talent.id).append('<input type="range" class="form-control-range slider" id="range_' + talent.id + '">\
                                                                      <span id="slider_value_' + talent.id + '" class="slider_value">' + talent.range_value + '</span>');

                                $("#range_text_" + talent.id).html("Offline");
                            };
                            // Update range value
                            $("#range_" + talent.id).val(talent.range_value);
                            $("#range_" + talent.id).attr('min', talent.range_min);
                            $("#range_" + talent.id).attr('max', talent.range_max);
                            $("#slider_value_" + talent.id).html(talent.range_value);
                            // $("#range_text_" + talent.id).html(talent.name);
                            // console.log(talent);
                            update_range(talent);

                            if (talent.in_transition){
                                // console.log('in transistions ');
                                $("#range_" + talent.id).attr('disabled', true);
                            } else {
                                // console.log('not transistions');
                                $("#range_" + talent.id).removeAttr("disabled");
                            };   
                           
                        };
                        break;
                    default:
                        return
                };
                return
               
            case 'devicestream':
                var device = ws_payload.data
                switch(ws_action){

                    case 'list':
                    case 'create':
                    case 'delete':
                        break;
                    case 'update':
                    case 'retrieve':
                        // console.log(`[message] Data received from server: ${event.data}`);
                        if (!obj_in_list(device, subscriptions.devices)){
                            console.log('subing to device ' + device.id);
                            send_format_data('devicestream', 'subscribe_instance', [], device.id);
                            subscriptions.devices.push(device);
                            devices_list.push(device);
                            // Get updated on_line status
                            send_format_data('driverstream', "retrieve", [], device.driver.id);
                            
                        };
                        // if (!subscriptions.drivers.includes(device.driver)){
                        //     console.log('subing to driver ' + device.driver.id)
                        //     send_format_data('driverstream', 'subscribe_instance', [], device.driver)
                        //     subscriptions.drivers.push(device.driver)
                        //     send_format_data('driverstream', "retrieve", [], device.driver);
                        //     // We don't need to do anything with the driver but subscribe so no need to retrieve
                        // }
                        // Add device  and driver for offline monitoring
                        if (!obj_in_list(device, devices_list)){
                            devices_list.push(device);
                            // device_drivers_id_list.push(device.driver);
                        };
                        if (device.is_online){
                            offline_devices.delete(device.id);
                        } else {
                            offline_devices.add(device.id);
                        };
                        update_online_offline();
                        break;
                    default:
                       return;
                };
                return

            case 'driverstream':
                var driver = ws_payload.data
                switch(ws_action){

                    case 'list':
                    case 'create':
                    case 'delete':
                        break;
                    case 'update':
                    case 'retrieve':

                        if (!obj_in_list(driver, subscriptions.drivers)){
                            // We need to get the driver info
                            console.log('subing to driver ' + driver.id);
                            send_format_data('driverstream', 'subscribe_instance', [], driver.id);
                            subscriptions.drivers.push(driver);
                            drivers_list.push(driver);
                            
                        };

                        if (!obj_in_list(driver, drivers_list)){
                            drivers_list.push(driver);
                        };
                        if (driver.is_online){
                            offline_drivers.delete(driver.id);
                        } else {
                            offline_drivers.add(driver.id);
                        };
                        update_online_offline();
                        break;
                    default:
                        return;
                };
                return

            default:
                return
        }
        
 
       
    }
    $(".button_container").click(function() {
        var button_id = event.target.id.replace('button_text_', '').replace('button_', '');
        // console.log("clicked " + button_id);
        // console.log(talents_list)
        talents_list.forEach(function(talent){
            // talent_local_states[talents_list.indexOf(talent)] = !talent_local_states[talents_list.indexOf(talent)]
            if (talent.id == button_id){
                // console.log("Talent State: " + talent.name + " " + talent.state)
                if ($('#button_' + talent.id).hasClass('button_error')){
                    console.log('This button is disabled')
                    show_failure(talent.device.name + ' ' + talent.name + ' is currently off line')
                    return
                }

                if (talent.state){
                    if (!talent.set_off){
                        // console.log('sending set_off')
                        
                        send_format_data('talentstream', 'patch', {"set_off": true}, talent.id);
                    }
                }else {
                    if (!talent.set_on){
                        // console.log('sending set_on')
                        send_format_data('talentstream', 'patch', {"set_on": true}, talent.id);
                    }
                }    
            }

        })        
    });
    // Feedback when disabled
    $(".range_container").click(function() {
        var range_id = event.target.id.replace('range_text_', '').replace('range_', '');
        var range_val = $(event.target).val()
        // console.log('range clicked ' + range_id);
        talents_list.forEach(function(talent){
            // talent_local_states[talents_list.indexOf(talent)] = !talent_local_states[talents_list.indexOf(talent)]
            if (talent.id == range_id){
                if ($('#range_' + talent.id).is(':disabled')){
                    // console.log('This button is disabled')
                    show_failure(talent.device.name + ' ' + talent.name + ' is currently off line')
                    return
                }
            }
        }
        );

    });
    $(".range_container").on('change', function () {
        var range_id = event.target.id.replace('range_text_', '').replace('range_', '');
        var range_val = $(event.target).val()
        // console.log("clicked " + range_id + ' value ' + range_val);
        talents_list.forEach(function(talent){
            // talent_local_states[talents_list.indexOf(talent)] = !talent_local_states[talents_list.indexOf(talent)]
            if (talent.id == range_id){
                console.log('Sending ' + range_val + " to " + talent.name)
                send_format_data('talentstream', 'patch', {"requested_range_value": range_val}, talent.id);
                $('#slider_value_' + talent.id).html(range_val);
            }
        })

    })
    //range
    $(".range_container").on('change', function () {
        // console.log('in range')
        var range_id = event.target.id.replace('range_text_', '').replace('range_', '');
        talents_list.forEach(function(talent){
            // talent_local_states[talents_list.indexOf(talent)] = !talent_local_states[talents_list.indexOf(talent)]
            if (talent.id == range_id){
                // update_range(talent);
            }
        })
        // var val = ($(event.target).val() - $(event.target).attr('min')) / ($(event.target).attr('max') - $(event.target).attr('min'));
        // var percent = val * 100;

        // $(event.target).css('background-image',
        //     '-webkit-gradient(linear, left top, right top, ' +
        //     'color-stop(' + percent + '%, #2A9D8F), ' +
        //     'color-stop(' + percent + '%, #264653)' +
        //     ')');

        // $(event.target).css('background-image',
        //     '-moz-linear-gradient(left center, #2A9D8F 0%, #2A9D8F ' + percent + '%, #264653 ' + percent + '%, #264653 100%)');
    });


    function send_format_data(stream, action, data, pk){
        // action retrieve subscribe
        request_id += 1
        console.log('sending: action:', action, 'data:', data, 'pk:',  pk, 'request_id:', request_id)
        ws.send(JSON.stringify({'stream': stream, 'payload': {"action": action, "request_id": 42, 'data': data, "pk": pk}}));
    };

    function obj_in_list(my_obj, my_list){
        var in_list = false;
        my_list.forEach(function(list_obj){
            if (list_obj.id == my_obj.id){
                in_list = true;
            }
        })
        return in_list;
    }

    function update_online_offline(){
        // console.log(offline_devices);
        // console.log(offline_drivers);
        // console.log(talents_list);
        // console.log(devices_list);
        // console.log(drivers_list);
        talents_list.forEach(function(talent){
            
            if (offline_devices.has(talent.device.id)){
                // Mark it offline and break out
                if(!talent.is_range){
                    $("#button_" + talent.id).addClass("button_error");
                } else {
                    $("#range_" + talent.id).attr('disabled', true);
                    // disable_range(talent, true);
                }
                // console.log("device marked " + talent.name + " offline")
                return;
            } else {
                if(!talent.is_range){
                    $("#button_" + talent.id).removeClass("button_error");
                } else {
                    $("#range_" + talent.id).removeAttr('disabled');
                    // disable_range(talent, false)
                }
                // console.log("device marked " + talent.name + " online")
            }
            devices_list.forEach(function (device) {
                if (device.id == talent.device.id){
                    // Only check if this device is the talents device
                
                    if (offline_drivers.has(device.driver.id)){
                        // Mark it offline and break out
                        if(!talent.is_range){
                            $("#button_" + talent.id).addClass("button_error");
                        } else {
                            $("#range_" + talent.id).attr('disabled', true);
                            // disable_range(talent, true)
                        }
                        // console.log("driver marked " + talent.name + " offline")
                    } else {
                        if(!talent.is_range){
                            $("#button_" + talent.id).removeClass("button_error");
                        } else {
                            $("#range_" + talent.id).removeAttr('disabled');
                            // disable_range(talent, false)
                        }
                        // console.log("driver marked " + talent.name + " offline")
                    }
                }
            });

        });
    };


});



// $(".form-control-range").on('input', function(){
//   var range_name = $(this).attr("id").substring(6);  // remove the range_
//   console.log(range_name);
//   console.log($(this).val())
//   var idx = ranges.indexOf(range_name)
//   console.log('talentstream ' + 'patch '+  "requested_range_value:  " +  $(this).val() + ' ' + range_ids[idx])
//   socket.send(format_patch_data('talentstream', 'patch', {"requested_range_value": $(this).val()}, range_ids[idx])); 
// });    


function updateOrfalse(obj, list) {
    // This fun little function checks if an object exists in the list, and if it does it updates it.
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].id === obj.id) {
            list.splice(i, 1, obj)
            // console.log('updated: ')
            // console.log(obj)
            return true;
        }
    }

    return false;
}


function show_success(message){
    $("div.success").html(message);
    $("div.success").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
};

function show_failure(message){
    $("div.failure").html(message);
    $("div.failure").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
};

function show_warning(message){
    $("div.warning").html(message);
    $("div.warning").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
};

// function show_failure(message){
//     $("div.danger").html(message);
//     $("div.danger").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
// };

function update_range(talent){
    // console.log('in update range')
    range = $("#range_" + talent.id) 
    var val = (range.val() - range.attr('min')) / (range.attr('max') - range.attr('min'));
    var percent = val * 100;

    
    // console.log(talent)
    if (talent.disable_if_in_transition && talent.in_transition){
        // console.log('in update range transistions');
        update_range_colors(talent, 'transistion');
        // document.documentElement.style.setProperty('--range-fill-color', '#e9c46a');
        // document.documentElement.style.setProperty('--range-background-color-color', '#e9c46a');
        // document.documentElement.style.setProperty('--range-thumb-color', '#e9c46a');
        // document.documentElement.style.setProperty('--range-fill-color', '#e9c46a');
        // document.documentElement.style.setProperty('--range-background-color-color', '#e9c46a');
        // document.documentElement.style.setProperty('--range-thumb-color', '#e9c46a');
        // // range.css('background-image',
        // //     '-webkit-gradient(linear, left top, right top, ' +
        //     'color-stop(' + percent + '%, var(--range-fill-color)), ' +
        //     'color-stop(' + percent + '%, var(--range-background-color))' +
        //     ')');

        // range.css('background-image',
        //     '-moz-linear-gradient(left center,  var(--range-fill-color) 0%,  var(--range-fill-color) ' + percent + '%, var(--range-background-color) ' + percent + '%, var(--range-background-color) 100%)');


        


    } else {
        // console.log('in update range normal');
        update_range_colors(talent, 'normal');
        // document.documentElement.style.setProperty('--range-fill-color', '#2A9D8F');
        // document.documentElement.style.setProperty('--range-background-color', '#264653');
        // document.documentElement.style.setProperty('--range-thumb-color', '#264653');
        // range.css('background-image',
        //     '-webkit-gradient(linear, left top, right top, ' +
        //     'color-stop(' + percent + '%, #2A9D8F), ' +
        //     'color-stop(' + percent + '%, #264653)' +
        //     ')');

        // range.css('background-image',
        //     '-moz-linear-gradient(left center,  #2A9D8F 0%, #2A9D8F ' + percent + '%, #264653 ' + percent + '%, #264653 100%)');
    }
}


function update_range_colors(talent, status){
    range = $("#range_" + talent.id) 
    var val = (range.val() - range.attr('min')) / (range.attr('max') - range.attr('min'));
    var percent = val * 100;
    switch(status){
        case 'normal':
            document.documentElement.style.setProperty('--range-fill-color', '#2A9D8F');
            document.documentElement.style.setProperty('--range-background-color', '#264653');
            document.documentElement.style.setProperty('--range-thumb-color', '#264653');
            range.css('background-image',
                '-webkit-gradient(linear, left top, right top, ' +
                'color-stop(' + percent + '%, #2A9D8F), ' +
                'color-stop(' + percent + '%, #264653)' +
                ')');

            range.css('background-image',
                '-moz-linear-gradient(left center,  #2A9D8F 0%,  #2A9D8F ' + percent + '%, #264653 ' + percent + '%, #264653 100%)');


            break;
        case 'transistion':
            document.documentElement.style.setProperty('--range-fill-color', '#e9c46a');
            document.documentElement.style.setProperty('--range-background-color-color', '#e9c46a');
            document.documentElement.style.setProperty('--range-thumb-color', '#e9c46a');
            range.css('background-image',
                '-webkit-gradient(linear, left top, right top, ' +
                'color-stop(' + percent + '%, #e9c46a), ' +
                'color-stop(' + percent + '%, #e9c46a)' +
                ')');

            range.css('background-image',
                '-moz-linear-gradient(left center,  #e9c46a 0%,  #e9c46a ' + percent + '%, #e9c46a ' + percent + '%, #e9c46a 100%)');


            break;
        case 'disable':
            break;
        default:
            console.log('Update range colors was called with: ' + status + ' but I dont know what to do with that');
            return;
    }
    // console.log('setting css')
    // range.css('background-image',
    //     '-webkit-gradient(linear, left top, right top, ' +
    //     'color-stop(' + percent + '%, var(--range-fill-color)), ' +
    //     'color-stop(' + percent + '%, var(--range-background-color))' +
    //     ')');

    // range.css('background-image',
    //     '-moz-linear-gradient(left center,  var(--range-fill-color) 0%,  var(--range-fill-color) ' + percent + '%, var(--range-background-color) ' + percent + '%, var(--range-background-color) 100%)');



// input[type=range]::-webkit-slider-thumb {
//     -webkit-appearance: none !important;
//     background-color: #2A9D8F;
//     height: 50px;
//     width: 13px;
//     /*border-radius: 50%;*/
// }

// input[type=range]::-moz-range-thumb {
//     -moz-appearance: none !important;
//     background-color: #2A9D8F;
//     border: none;
//     height: 13px;
//     width: 13px;
//     border-radius: 50%;
// }

        

}