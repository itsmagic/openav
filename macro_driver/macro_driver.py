from driver_base import DriverBase, Talent
from macro_thread import MacroThread
import time
import datetime
from pydispatch import dispatcher
import os
import sys
import logging
import signal
from croniter import croniter


logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=int(os.environ.get('LOG_LEVEL', 50)),
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)

logger = logging.getLogger("MacroDriver")
logging.getLogger("chardet.charsetprober").disabled = True


class MacroAction:
    def __init__(self, config):
        self.name = None
        self.id = None
        for key in config:
            if key == 'talent' and config[key] is not None:
                self.talent = Talent(config[key])
            else:
                setattr(self, key, config[key])

    def __str__(self):
        return f"MacroAction: {self.name}"

    def __repr__(self):
        return f"MacroAction: {self.name}"


class Macro:
    def __init__(self, config):
        self.name = None
        self.id = None
        self.macro_actions = []
        for key in config:
            if key == 'macro_actions':
                for action_conf in config['macro_actions']:
                    self.macro_actions.append(MacroAction(action_conf))
            elif key == 'talent' and config[key] is not None:
                self.talent = Talent(config[key])
            else:
                setattr(self, key, config[key])

    def get_macro_action_by_name(self, name):
        for macro_action in self.macro_actions:
            if macro_action.name == name:
                return macro_action
        logger.critical(f"{self}: Unable to get macro_action by name: {name}")
        return None

    def get_macro_action_by_id(self, id):
        for macro_action in self.macro_actions:
            if macro_action.id == id:
                return macro_action
        logger.critical(f"{self}: Unable to get macro_action by id: {id}")
        return None

    def __str__(self):
        return f"Macro: {self.name}"

    def __repr__(self):
        return f"Macro: {self.name}"


class ScheduledEvent:
    def __init__(self, config):
        self.name = None
        self.id = None
        self.cron_valid = False
        self.iter = None
        self.next_execution = None
        for key in config:
            if key == 'talent' and config[key] is not None:
                self.talent = Talent(config[key])
            elif key == 'cron_schedule':
                if croniter.is_valid(config[key]):
                    self.cron_valid = True
                    base = datetime.datetime.now()
                    self.iter = croniter(config[key], base)
                    self.next_execution = self.iter.get_next(datetime.datetime)
            else:
                setattr(self, key, config[key])

    def __str__(self):
        return f"ScheduledEvent: {self.name}"

    def __repr__(self):
        return f"ScheudledEvent: {self.name}"


class MacroDriver(DriverBase):
    """The Macro Driver Thread"""

    def __init__(self):
        self.local_macros = []
        self.connect_to_macrostream = False
        self.local_scheduledevents = []
        self.connect_to_scheduledeventstream = False
        self.request_id_dict = {}
        self.macro_actions_threads = []
        self.loop_count = 0

        dispatcher.connect(self.thread_finished, signal="MacroThread Finished")
        dispatcher.connect(self.thread_send, signal="MacroThread ws_send")
        dispatcher.connect(self.ws_status_monitor, signal="Websocket Status")
        super().__init__()

    def ws_status_monitor(self, status):
        if not status['connected']:
            self.connect_to_macrostream = False
            self.connect_to_scheduledeventstream = False

    def on_talent_change(self, talent):
        pass

    def on_updated_driverconfig(self, config):
        pass

    def periodic(self):
        # Runs every loop after connecting

        # Make sure we are online
        local_device = self.get_device_by_unique_id("MacroDevice")
        if local_device:
            if not local_device.online:
                self.device_update(device=local_device, online=True)
        else:
            logging.critical(f"Unable to find MacroDevice")

        if not self.connect_to_macrostream:
            self._ws_send(stream='macrostream', payload={"action": "monitor", "request_id": self._get_request_id()})
            self._ws_send(stream='macrostream', payload={"action": "list", "request_id": self._get_request_id()})

        # Check and run any scheduled events
        if not self.connect_to_scheduledeventstream:
            self._ws_send(stream='scheduledeventstream', payload={"action": "monitor", "request_id": self._get_request_id()})
            self._ws_send(stream='scheduledeventstream', payload={"action": "list", "request_id": self._get_request_id()})

        for event in self.local_scheduledevents:
            # logging.critical(f"Next execution: {event.next_execution} current time: {datetime.datetime.now()}")
            # logging.critical(f"Checking enabled: {event.enabled} cron_valid: {event.cron_valid}")
            if event.enabled and event.cron_valid and event.talent:
                if event.next_execution <= datetime.datetime.now():
                    event.next_execution = event.iter.get_next(datetime.datetime)
                    self._ws_send(stream="talentstream", payload={"action": "patch",
                                                                 "data": {'set_on': True},
                                                                 "pk": event.talent.id,
                                                                 "request_id": self._get_request_id()})
                    if event.one_shot:
                        self._ws_send(stream="scheduledeventstream", payload={"action": "patch",
                                                                             "data": {'enabled': False},
                                                                             "pk": event.id,
                                                                             "request_id": self._get_request_id()})

        self.loop_count += 1
        # logger.critical(f"Loop count: {self.loop_count}")
        if self.loop_count > 9:
            self.loop_count = 0
            self._ws_send(stream='watchdogstream', payload={"action": "check_watchdog", 'data': {}, "request_id": self._get_request_id()})

    def on_driverconfig_set(self, config):
        my_json = """[{
	"description": "MacroDevice for Macro Actions",
	"name": "MacroDevice",
	"unique_id": "MacroDevice",
	"online": true
}]"""
        self._ws_send(stream='driverconfigstream',
                     payload={"action": "patch",
                              "data": {'config_value': my_json},
                              'pk': config['id'],
                              'request_id': self._get_request_id()})

    def create_macro_actions_thread(self, talent, macro):
        new_thread = MacroThread(talent, macro)
        self.macro_actions_threads.append((talent, new_thread))
        logger.debug(f"Threads after add: {self.macro_actions_threads}")
        new_thread.daemon = True
        new_thread.start()

    def thread_send(self, stream, payload):
        self._ws_send(stream=stream, payload=payload)

    def thread_finished(self, talent):
        logger.debug(f"Finished {talent}")
        self.talent_update(talent=talent, state=False, in_transition=False)
        for i, item in enumerate(self.macro_actions_threads):
            if item[0] == talent:
                self.macro_actions_threads.pop(i)
        logger.debug(f"Threads after remove: {self.macro_actions_threads}")

    def on_updated_driverconfig(self, config):
        return

    def on_talentstream(self, ws_payload, ws_action):
        # Talentstream
        logger.debug(f"In talentstream processing")
        logger.debug(f"ws_payload: {ws_payload}")

        if ws_action == "create":
            talent = Talent(ws_payload['data'])
            # Make sure we are subscribed
            self._subscribe_instance(stream="talentstream", item_id=talent.id)
            # We need to make sure the locally stored macro is updated with the talent
            macro = self.request_id_dict[ws_payload['request_id']]
            # patch the web macro
            self._ws_send(stream='macrostream',
                         payload={'action': 'patch',
                                  'data': {},
                                  'talent': talent.id,
                                  'pk': macro.id,
                                  "request_id": self._get_request_id()})
            # self._ws_send(stream='macrostream', payload={"action": "retrieve", "pk": macro.id, "request_id": self._get_request_id()})
            for local_macro in self.local_macros:
                if local_macro.id == macro.id:
                    local_macro.talent = talent

        if ws_action == "update":
            talent = Talent(ws_payload['data'])
            if talent.set_on:
                # need to clear this
                self.talent_clear(talent=talent, clear="set_on")
                self.talent_update(talent=talent, state=True, in_transition=True)
                logger.info(f"Patching talent set_on to False {talent.id}")
                # We need to create a thread so we can cancel if necessary
                for macro in self.local_macros:
                    if macro.talent.id == talent.id:
                        for i, item in enumerate(self.macro_actions_threads):
                            if item[0] == talent:
                                logger.error(f"Not starting macro when macro is already running: {macro}")
                                return
                        self.create_macro_actions_thread(talent=talent, macro=macro)

            elif talent.set_off:
                # Need to attempt to cancel thread
                for i, item in enumerate(self.macro_actions_threads):
                    # item is a tuple (talent, thread)
                    if item[0].id == talent.id:
                        item[1].shutdown = True

    def on_macrostream(self, ws_payload, ws_action):
        # Macrostream
        logger.debug(f"In macrostream processing")
        logger.debug(f"ws_payload: {ws_payload}")
        if ws_action == "list":
            self.connect_to_macrostream = True
            self.local_macros = []
            macro_list = ws_payload['data']
            # Update local list
            for macro_conf in macro_list:
                macro = Macro(macro_conf)
                self.local_macros.append(macro)
                # logger.critical(f"macro: {macro['talent']} {bool(macro['talent'])}")
                if macro.talent is None:
                    # Need to create a talent
                    request_id = f"T{self._get_request_id()}"
                    self.request_id_dict[request_id] = macro
                    self._create_talent(name=macro.name,
                                       description=f"Created to control macro: {macro.name}",
                                       state=False,
                                       device_id=self.get_device_by_unique_id(unique_id="MacroDevice").id,
                                       request_id=request_id)
                else:
                    self._subscribe_instance(stream="talentstream", item_id=macro.talent.id)
                    # clear talent
                    self._ws_send(stream="talentstream", payload={"action": "patch",
                                                                 "data": {'set_on': False,
                                                                          'set_off': False,
                                                                          'state': False,
                                                                          'in_transition': False},
                                                                 "pk": macro.talent.id,
                                                                 "request_id": self._get_request_id()})
        if ws_action == "create":
            macro = Macro(ws_payload["data"])
            self.local_macros.append(macro)
            # Need to create a talent to contorl this macro
            request_id = f"T{self._get_request_id()}"
            self.request_id_dict[request_id] = macro

            self._create_talent(name=macro.name,
                               description=f"Created to control macro: {macro.name}",
                               state=False,
                               device_id=self.get_device_by_unique_id(unique_id="MacroDevice").id,
                               request_id=request_id)
            # logger.critical(f"before create_talent")

        if ws_action in ["update", "retrieve", "patch"]:
            # We use the local_macro list to store the talents if they have changed we need to update it
            updated_macro = Macro(ws_payload["data"])

            for i, macro in enumerate(self.local_macros):
                # logger.critical(f"updated_macro: {updated_macro.id} {macro.id}")
                if macro.id == updated_macro.id:
                    self.local_macros[i] = updated_macro
                    if not macro.talent:
                        request_id = f"T{self._get_request_id()}"
                        self.request_id_dict[request_id] = macro
                        self._create_talent(name=macro.name,
                                           description=f"Created to control macro: {macro.name}",
                                           state=False,
                                           device_id=self.get_device_by_unique_id(unique_id="MacroDevice").id,
                                           request_id=request_id)

        if ws_action == "delete":
            delete_macros = []
            for macro in self.local_macros:
                if macro.name == ws_payload["data"]['name']:
                    delete_macros.append(macro)
            for macro in delete_macros:
                self.local_macros.pop(self.local_macros.index(macro))

    def on_scheduledeventstream(self, ws_payload, ws_action):
        # logger.critical(f"In scheduledeventstream processing {ws_action}")
        # logger.critical(f"ws_payload: {pformat(ws_payload)}")
        if ws_action == "list":
            self.local_scheduledevents = []
            self.connect_to_scheduledeventstream = True
            events_list = ws_payload['data']
            # Update local list
            for event_conf in events_list:
                event = ScheduledEvent(event_conf)
                self.local_scheduledevents.append(event)

        if ws_action == "create":
            event = ScheduledEvent(ws_payload["data"])
            self.local_scheduledevents.append(event)

        if ws_action in ["update", "retrieve", "patch"]:
            # We use the local_macro list to store the talents if they have changed we need to update it
            updated_event = ScheduledEvent(ws_payload["data"])

            for i, event in enumerate(self.local_scheduledevents):
                # logger.critical(f"updated_macro: {updated_macro.id} {macro.id}")
                if event.id == updated_event.id:
                    self.local_scheduledevents[i] = updated_event

        if ws_action == "delete":
            for event in self.local_scheduledevents:
                if event.name == ws_payload["data"]['name']:
                    self.local_scheduledevents.pop(self.local_scheduledevents.index(event))
        # logger.critical(f"Current Local ScheduledEvents: {pformat(self.local_scheduledevents)}")


class ServerExit(Exception):
    pass


def service_shutdown(signum, frame):
    print('Caught signal %d' % signum)
    raise ServerExit


def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)

    my_driver = MacroDriver()
    my_driver.daemon = True
    my_driver.start()
    try:
        while True:
            time.sleep(1)
    except ServerExit:
        logger.critical('Shutting down')
        dispatcher.send(signal="Shutdown")
        my_driver.join()


if __name__ == '__main__':
    main()
