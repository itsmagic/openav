# from driver_registration import DriverRegistration
# from unittest.mock import patch, Mock
# import pytest
# import unittest
# import datastore
# import time


# @pytest.mark.skip("Very slow test")
# @patch('driver_registration.DriverRegistration.ws_send')
# @patch('driver_registration.DriverRegistration.start_ws')
# class TestDriverRegistration(unittest.TestCase):

#     def setUp(self):
#         my_conf = datastore.Config()
#         self.my_driver = DriverRegistration(my_conf)
#         # my_driver.conf.device_online = True
#         # setattr(self.my_driver.conf, 'device_id', 111)

#     @pytest.mark.skip("Very slow test")
#     def test_if_we_have_do_NOT_have_a_driver_id_send_create(self, mock_start_ws, mock_ws_send):
#         self.my_driver.start()
#         self.my_driver.ws_connected = True
#         time.sleep(2)
#         self.my_driver.shutdown = True
#         self.assertTrue(mock_ws_send.called)
#         self.assertEqual(len(mock_ws_send.mock_calls), 1)
#         # self.assertEqual(mock_ws_send.mock_calls, '')
#         name, args, kwargs = mock_ws_send.mock_calls[0]
#         self.assertEqual(kwargs['payload']['action'], 'create')

#     @pytest.mark.skip("Very slow test")
#     def test_if_we_have_do_have_a_driver_id_send_retrieve(self, mock_start_ws, mock_ws_send):
#         self.my_driver.conf.driver_id = '123'
#         self.my_driver.start()
#         self.my_driver.ws_connected = True
#         time.sleep(2)
#         self.my_driver.shutdown = True
#         self.assertTrue(mock_ws_send.called)
#         self.assertEqual(len(mock_ws_send.mock_calls), 1)
#         # self.assertEqual(mock_ws_send.mock_calls, '')
#         name, args, kwargs = mock_ws_send.mock_calls[0]
#         self.assertEqual(kwargs['payload']['action'], 'retrieve')

#     @patch('driver_registration.DriverRegistration.shelve_send')
#     def test_ws_incoming_create_stores_driver_id(self, mock_shelve_send, mock_start_ws, mock_ws_send):

#         message = '{"stream": "driverstream", "payload": {"errors": [], "data": {"name": "MacroDriver2", "owner": "macrodriver", "description": "", "is_online": true, "authorized": false, "id": 62, "driver_uuid": "4dfc1127-a34e-4013-8acc-38dafb518922", "devices": []}, "action": "create", "response_status": 201, "request_id": 42}}'
#         self.my_driver.ws_incoming(message)
#         self.assertEqual(self.my_driver.conf.driver_id, 62)
#         self.assertEqual(self.my_driver.conf.uuid, '4dfc1127-a34e-4013-8acc-38dafb518922')
#         self.assertTrue(len(mock_shelve_send.mock_calls), 2)
#         name, args, kwargs = mock_shelve_send.mock_calls[0]
#         self.assertEqual(kwargs['key'], 'uuid')
#         self.assertEqual(kwargs['data'], '4dfc1127-a34e-4013-8acc-38dafb518922')
#         name, args, kwargs = mock_shelve_send.mock_calls[1]
#         self.assertEqual(kwargs['key'], 'driver_id')
#         self.assertEqual(kwargs['data'], 62)

#     def test_retreives_if_not_authorized(self, mock_start_ws, mock_ws_send):
#         self.my_driver.conf.driver_id = '123'
#         self.my_driver.authorized = False
#         self.my_driver.ws_connected = True
#         self.my_driver.start()
#         time.sleep(2)
#         self.my_driver.shutdown = True
#         self.assertTrue(mock_ws_send.called)
#         self.assertEqual(len(mock_ws_send.mock_calls), 1)
#         # self.assertEqual(mock_ws_send.mock_calls, '')
#         name, args, kwargs = mock_ws_send.mock_calls[0]
#         self.assertEqual(kwargs['payload']['action'], 'retrieve')

#     def test_sends_update_watchdog_if_authorized(self, mock_start_ws, mock_ws_send):
#         self.my_driver.conf.driver_id = '123'
#         self.my_driver.authorized = True
#         self.my_driver.ws_connected = True
#         self.my_driver.start()
#         time.sleep(2)
#         self.my_driver.shutdown = True
#         self.assertTrue(mock_ws_send.called)
#         self.assertEqual(len(mock_ws_send.mock_calls), 1)
#         # self.assertEqual(mock_ws_send.mock_calls, '')
#         name, args, kwargs = mock_ws_send.mock_calls[0]
#         self.assertEqual(kwargs['payload']['action'], 'update_watchdog')

#     def test_sets_authorized_on_retrieve_if_uuid_matches(self, mock_start_ws, mock_ws_send):
#         message = '{"stream": "driverstream", "payload": {"errors": [], "data": {"name": "MacroDriver2", "owner": "macrodriver", "description": "", "is_online": true, "authorized": true, "id": 62, "driver_uuid": "4dfc1127-a34e-4013-8acc-38dafb518922", "devices": []}, "action": "retrieve", "response_status": 200, "request_id": 42}}'
#         self.my_driver.conf.uuid = "4dfc1127-a34e-4013-8acc-38dafb518922"
#         self.my_driver.ws_incoming(message)
#         self.assertEqual(self.my_driver.authorized, True)

#     def test_sets_not_authorized_on_retrieve_if_not_authorized(self, mock_start_ws, mock_ws_send):
#         message = '{"stream": "driverstream", "payload": {"errors": [], "data": {"name": "MacroDriver2", "owner": "macrodriver", "description": "", "is_online": true, "authorized": false, "id": 62, "driver_uuid": "4dfc1127-a34e-4013-8acc-38dafb518922", "devices": []}, "action": "retrieve", "response_status": 200, "request_id": 42}}'
#         self.my_driver.conf.uuid = "4dfc1127-a34e-4013-8acc-38dafb518922"
#         self.my_driver.ws_incoming(message)
#         self.assertEqual(self.my_driver.authorized, False)
