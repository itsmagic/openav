from unittest.mock import patch, Mock
import pytest
import unittest
import os
import json

from macro_driver import Macro
import macro_driver


@patch('macro_driver.MacroDriver.ws_send')
@patch('macro_driver.MacroDriver.create_talent')
class TestMacroStream(unittest.TestCase):

    def setUp(self):
        os.environ["LOG_LEVEL"] = "10"
        os.environ["WS_TOKEN"] = "Test"
        self.my_driver = macro_driver.MacroDriver()

    def test_create_macro_object_from_conf(self, mock_create_talent, mock_ws_send):

        macro_from_list_message = {'name': 'test1', 'description': '', 'macro_order': '0,1', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': None, 'id': 4}
        my_macro = Macro(macro_from_list_message)
        self.assertEqual(my_macro.db_id, 4)

        # Check if it contains macro actions?
        self.assertEqual(len(my_macro.macro_actions), 2)

        # Check if the macro actions are objects
        self.assertEqual(my_macro.macro_actions[0].name, 'Lamp1On')
        self.assertEqual(my_macro.macro_actions[0].talent.name, 'Lamp1')
        self.assertEqual(my_macro.macro_actions[0].talent.db_id, 353)


    def test_local_macros_created_on_list_action(self, mock_create_talent, mock_ws_send):
        # This test data has a talent assigned so no call should be made to create_talent
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': '', 'macro_order': '0,1', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': {'name': 'test1', 'description': 'Created to control macro: test1', 'created': '2021-01-02T07:18:53.803018+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 363, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'id': 4}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # There should be a talent listed for the macro
        self.assertTrue(self.my_driver.local_macros[0].talent is not None)

        # We should subscribe to this talent
        self.assertTrue(mock_ws_send.called)
        # We should not create a talent
        self.assertFalse(mock_create_talent.called)
    
    def test_create_talent_on_list(self, mock_create_talent, mock_ws_send):
        # This test data has no talent assigned so call should be made to create_talent
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': '', 'macro_order': '0,1', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': None, 'id': 4}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # There should not be a talent listed for the macro
        self.assertTrue(self.my_driver.local_macros[0].talent is None)

        # We should not subscribe to this talent
        # We should create a talent
        # description, args, kwargs = mock_ws_send.mock_calls[0]
        # self.assertEqual(description, "Created to control macro: test1")
        self.assertTrue(mock_create_talent.called)


    def test_update_updates_local_list(self, mock_create_talent, mock_ws_send):
        # First we add an item to the list
        # This test data has no talent assigned so call should be made to create_talent
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': '', 'macro_order': '0,1', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': {'name': 'test1', 'description': 'Created to control macro: test1', 'created': '2021-01-02T07:18:53.803018+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 363, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'id': 4}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # Only one macro should be created
        self.assertEqual(len(self.my_driver.local_macros), 1)
        # Check macro order
        self.assertEqual(self.my_driver.local_macros[0].macro_order, '0,1')

        # Then we update it..
        # This test data has a talent assigned so no call should be made to create_talent
        ws_payload = {'name': 'test1', 'description': '', 'macro_order': '0,2', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': {'name': 'test1', 'description': 'Created to control macro: test1', 'created': '2021-01-02T14:05:27.794401+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 364, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'id': 4, 'type': 'model.activity', 'action': 'update'}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='update')

        # The len should be the same
        self.assertEqual(len(self.my_driver.local_macros), 1)
        # The vaule should updated
        self.assertEqual(self.my_driver.local_macros[0].macro_order, '0,2')

    def test_delete_updates_local_list(self, mock_create_talent, mock_ws_send):
        # First we add an item to the list
        # This test data has no talent assigned so call should be made to create_talent
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': '', 'macro_order': '0,1', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': {'name': 'test1', 'description': 'Created to control macro: test1', 'created': '2021-01-02T07:18:53.803018+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 363, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'id': 4}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # Only one macro should be created
        self.assertEqual(len(self.my_driver.local_macros), 1)

        # Now we delete it
        ws_payload = {'name': 'test1', 'description': '', 'macro_order': '0', 'macro_actions': [], 'talent': None, 'id': 4, 'type': 'model.activity', 'action': 'delete'}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='delete')

        self.assertEqual(len(self.my_driver.local_macros), 0)

    def test_create_adds_to_local_list(self, mock_create_talent, mock_ws_send):
        # First we add an item to the list
        # This test data has no talent assigned so call should be made to create_talent
        ws_payload = {'name': 'test2', 'description': '', 'macro_order': '0', 'macro_actions': [], 'talent': None, 'id': 5, 'type': 'model.activity', 'action': 'create'}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='create')

        # Only one macro should be created
        self.assertEqual(len(self.my_driver.local_macros), 1)

    def test_create_creates_new_talent(self, mock_create_talent, mock_ws_send):
        # First we add an item to the list
        # This test data has no talent assigned so call should be made to create_talent
        ws_payload = {'name': 'test2', 'description': '', 'macro_order': '0', 'macro_actions': [], 'talent': None, 'id': 5, 'type': 'model.activity', 'action': 'create'}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='create')

        # We shold call create_talent
        # self.assertTrue(mock_create_talent.called)




@patch('macro_driver.MacroDriver.ws_send')
@patch('macro_driver.MacroDriver.create_talent')
class TestTalentStream(unittest.TestCase):

    def setUp(self):
        self.my_driver = macro_driver.MacroDriver()

    def test_create_message_subscribes_to_talent_and_updates_local_macro(self, mock_create_talent, mock_ws_send):

        # Create our macro
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': '', 'macro_order': '0,1', 'macro_actions': [{'name': 'Lamp1On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': None, 'id': 4}, {'name': 'Lamp2On', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 5}], 'talent': None, 'id': 4}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # Only one macro should be created
        self.assertEqual(len(self.my_driver.local_macros), 1)

        # Check that no talent is listed
        self.assertEqual(self.my_driver.local_macros[0].talent, None)

        # We should ws_send to create talent
        self.assertTrue(mock_create_talent.called)

        # We need to set the request_id dict
        self.my_driver.request_id_dict[101] = self.my_driver.local_macros[0]

        # We get the Talentstream create response,
        ws_payload = {'errors': [], 'data': {'name': 'test12', 'description': 'Created to control macro: test12', 'created': '2021-01-02T14:59:02.411833+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 372, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'action': 'create', 'response_status': 201, 'request_id': 101}
        self.my_driver.on_talentstream(ws_payload=ws_payload, ws_action='create')

        # we expect that the local macro will now list this as its talent
        self.assertEqual(self.my_driver.local_macros[0].talent.name, "test12")

    def test_update_message_calls_macro_actions(self, mock_create_talent, mock_ws_send):

        # Create our macro
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': 'this is test 1', 'macro_order': '0,1', 'macro_actions': [{'name': 'lamp1on', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 6}, {'name': 'lamp2on', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 7}], 'talent': {'name': 'test1', 'description': 'Created to control macro: test1', 'created': '2021-01-03T06:49:10.414462+10:00', 'set_on': True, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 374, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'id': 16}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # Only one macro should be created
        self.assertEqual(len(self.my_driver.local_macros), 1)

        # We need to set the request_id dict
        self.my_driver.request_id_dict[101] = self.my_driver.local_macros[0]

        # We get the Talentstream create response,
        ws_payload = {'errors': [], 'data': {'name': 'test12', 'description': 'Created to control macro: test12', 'created': '2021-01-02T14:59:02.411833+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 372, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'action': 'create', 'response_status': 201, 'request_id': 101}
        self.my_driver.on_talentstream(ws_payload=ws_payload, ws_action='create')

        # Now that the setup is complete
        # Talent set_on cleared
        # ws_payload = {'errors': [], 'data': {'name': 'test12', 'description': 'Created to control macro: test12', 'created': '2021-01-02T14:59:02.411833+10:00', 'set_on': True, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 372, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'action': 'update', 'response_status': 201, 'request_id': 101}
        # self.my_driver.on_talentstream(ws_payload=ws_payload, ws_action='update')
        # _, args, kwargs = mock_ws_send.mock_calls[2]
        # self.assertEqual(kwargs['payload']['action'], 'patch')

        # # now should call each talent in macro
        # ids_called = []
        # _, args, kwargs = mock_ws_send.mock_calls[3]
        # ids_called.append(kwargs['payload']['pk'])
        # _, args, kwargs = mock_ws_send.mock_calls[4]
        # ids_called.append(kwargs['payload']['pk'])
        # self.assertEqual(ids_called, [372, 353])


