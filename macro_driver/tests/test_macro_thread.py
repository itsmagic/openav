from unittest.mock import patch, Mock
import pytest
import unittest
import os
import json

from macro_driver import Macro
import macro_driver


@patch('macro_driver.MacroDriver.create_macro_actions_thread')
@patch('macro_driver.MacroDriver.ws_send')
class TestMacroThreadControl(unittest.TestCase):

    def setUp(self):
        os.environ["LOG_LEVEL"] = "10"
        os.environ["WS_TOKEN"] = "Test"
        self.my_driver = macro_driver.MacroDriver()

    def test_create_macro_thread_when_set_on(self, mock_ws_send, mock_create_macro_actions_thread):

        # Create our macro
        ws_payload = {'errors': [], 'data': [{'name': 'test1', 'description': 'this is test 1', 'macro_order': '0,1', 'macro_actions': [{'name': 'lamp1on', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp1', 'description': '', 'created': '2020-12-31T12:58:00.642523+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': True, 'in_transition': False, 'disable_if_in_transition': False, 'id': 353, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 6}, {'name': 'lamp2on', 'requested_range_value': 0, 'set_on': True, 'set_off': False, 'talent': {'name': 'Lamp2', 'description': '', 'created': '2020-12-31T12:58:00.682405+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 354, 'device': {'name': 'ArlecTwin Power Point1', 'unique_id': 'ArlecTwin1', 'description': 'Dual controllable power point', 'is_online': False, 'id': 104, 'driver': {'name': 'MQTTDriver', 'owner': 'mqtt_user', 'description': '', 'is_online': False, 'authorized': True, 'id': 110, 'driver_uuid': '588af634-0306-48e2-9ff0-65b85e8c4aea', 'devices': [104, 105, 106, 107]}}}, 'id': 7}], 'talent': {'name': 'test1', 'description': 'Created to control macro: test1', 'created': '2021-01-03T06:49:10.414462+10:00', 'set_on': True, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 374, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'id': 16}], 'action': 'list', 'response_status': 200, 'request_id': 42}
        self.my_driver.on_macrostream(ws_payload=ws_payload, ws_action='list')

        # Only one macro should be created
        self.assertEqual(len(self.my_driver.local_macros), 1)

        # We need to set the request_id dict
        self.my_driver.request_id_dict[101] = self.my_driver.local_macros[0]

        # We get the Talentstream create response,
        ws_payload = {'errors': [], 'data': {'name': 'test12', 'description': 'Created to control macro: test12', 'created': '2021-01-02T14:59:02.411833+10:00', 'set_on': False, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 372, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'action': 'create', 'response_status': 201, 'request_id': 101}
        self.my_driver.on_talentstream(ws_payload=ws_payload, ws_action='create')

        # Now that the setup is complete
        # Talent set_on cleared
        ws_payload = {'errors': [], 'data': {'name': 'test12', 'description': 'Created to control macro: test12', 'created': '2021-01-02T14:59:02.411833+10:00', 'set_on': True, 'set_off': False, 'is_range': False, 'range_value': 0, 'requested_range_value': 0, 'range_min': 0, 'range_max': 100, 'state': False, 'in_transition': False, 'disable_if_in_transition': False, 'id': 372, 'device': {'name': 'MacroDevice', 'unique_id': 'MacroDevice', 'description': 'MacroDevice for Macro Actions', 'is_online': False, 'id': 109, 'driver': {'name': 'Macro Driver', 'owner': 'macrodriver', 'description': '', 'is_online': True, 'authorized': True, 'id': 114, 'driver_uuid': 'a297d0f4-030d-427a-8f5b-96713c20a5f7', 'devices': [109]}}}, 'action': 'update', 'response_status': 201, 'request_id': 101}
        self.my_driver.on_talentstream(ws_payload=ws_payload, ws_action='update')

        self.assertEqual(self.my_driver.macro_actions_threads, [])
        # We need to create a thread

        # and pass it macro_actions, we also need to add it to the macro_actions_list
        # self.assertTrue(mock_create_macro_actions_thread.called)
        # args, kwargs = mock_create_macro_actions_thread.calls[0]
        # self.assertEqual(kwargs['macro_order'], 'patch')
        # self.assertEqual(len(self.my_driver.macro_actions_threads), 1)


        # _, args, kwargs = mock_ws_send.mock_calls[2]
        # self.assertEqual(kwargs['payload']['action'], 'patch')

        # # now should call each talent in macro
        # ids_called = []
        # _, args, kwargs = mock_ws_send.mock_calls[3]
        # ids_called.append(kwargs['payload']['pk'])
        # _, args, kwargs = mock_ws_send.mock_calls[4]
        # ids_called.append(kwargs['payload']['pk'])
        # self.assertEqual(ids_called, [372, 353])