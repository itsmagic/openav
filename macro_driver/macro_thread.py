from pydispatch import dispatcher
from threading import Thread
import time
import logging


logger = logging.getLogger("MacroThread")


class MacroThread(Thread):
    """The SVSi Driver Thread"""

    def __init__(self, talent, macro):
        """Init the worker class"""
        self.shutdown = False
        self.talent = talent
        self.macro = macro
        self.ws_connected = False
        self.authorized = False

        dispatcher.connect(self.shutdown_signal, signal="Shutdown")
        # dispatcher.connect(self.ws_incoming, signal="Websocket Incoming")
        # dispatcher.connect(self.ws_status, signal="Websocket Status")

        Thread.__init__(self, name=macro.name)

    def shutdown_signal(self):
        self.shutdown = True

    def run(self):
        try:
            for item in self.macro.macro_order.split(','):
                if 'delay' in item:
                    delay = int(item.split(':')[1])
                    for i in range(delay):
                        if self.shutdown:
                            # Either skip the delays and finish the macro or just quit
                            if self.macro.remove_delays_on_cancel:
                                break
                            else:
                                dispatcher.send(signal='MacroThread Finished', talent=self.talent)
                                return
                        time.sleep(.1)
                    continue
                macro_action = self.macro.macro_actions[int(item)]
                logger.info(f"Patching talent {macro_action.talent.id} to set_on: {macro_action.set_on} set_off:{macro_action.set_off} requested_range_value: {macro_action.requested_range_value}")
                dispatcher.send(signal="MacroThread ws_send",
                                stream="talentstream",
                                payload={"action": "patch",
                                         "data": {'set_on': macro_action.set_on,
                                                  'set_off': macro_action.set_off,
                                                  'requested_range_value': macro_action.requested_range_value},
                                         "pk": macro_action.talent.id,
                                         "request_id": 42})
        except Exception as error:
            logger.critical(f"Unable to process macro, macro_order is {self.macro.macro_order} error was: {error}")
        dispatcher.send(signal='MacroThread Finished', talent=self.talent)

