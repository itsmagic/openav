Provisioning a new site
=======================
## Required packages:
* nginx
* redis
* Python 3.6
* virtualenv + pip
* Git
eg, on Ubuntu:
 sudo add-apt-repository ppa:fkrull/deadsnakes
 sudo apt-get install nginx git python38 python3.8-venv
## Nginx Virtual Host config
* see nginx.template.conf
* replace SITENAME with, e.g., staging.my-domain.com
## Systemd service
* see gunicorn-systemd.template.service
* replace SITENAME with, e.g., staging.my-domain.com
## Sudo
* %sudo  ALL=NOPASSWD: /bin/systemctl restart openav.service
* %sudo  ALL=NOPASSWD: /bin/systemctl restart macro_driver.service
## Folder structure:
Assume we have a user account at /home/username
/home/username
└── sites
 └── SITENAME
 ├── database
 ├── source
 ├── static
 └── virtualenv